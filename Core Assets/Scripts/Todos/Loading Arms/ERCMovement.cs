﻿using UnityEngine;
using System.Collections;

public class ERCMovement : MonoBehaviour {

	Quaternion posicionVertical;

	// Use this for initialization
	void Start () {
		posicionVertical = transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {

		Quaternion posicionNueva = transform.rotation;

		posicionNueva.z = posicionVertical.z;
		posicionNueva.x = posicionVertical.x;

		transform.rotation = posicionNueva;
	}
}
