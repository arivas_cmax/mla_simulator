﻿using UnityEngine;
using System.Collections;

public class WaterPlane : MonoBehaviour {

	public GameObject waterPlane;

	private TritonUnity triton;

	// Use this for initialization
	void Start () {
		triton = (TritonUnity)(GameObject.FindObjectOfType(typeof(TritonUnity)));

		if (waterPlane == null) {
			waterPlane = GameObject.Find ("WaterPlane");
		}
	}

	void OnPostRender() {
		if(waterPlane != null) {
			if (triton.headless) {
				waterPlane.GetComponent<Renderer>().enabled = false;
			} else {
				waterPlane.GetComponent<Renderer>().enabled = true;
			}
		}
	}
}
