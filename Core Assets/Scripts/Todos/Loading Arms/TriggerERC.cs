﻿using UnityEngine;
using System.Collections;

public class TriggerERC : MonoBehaviour {
	public GameObject controller;
    public static bool hasEntered = false;
    LoadingArmController scriptActual = null;

	// Use this for initialization
	void Start () {
        hasEntered = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            scriptActual.enableERC = false;
            hasEntered = false;
        }
	}

	void OnTriggerExit(Collider other){
		//Debug.Log("Entered Red");
		if(other.gameObject.tag == "ArmCollider")
		{
			LoadingArmController script = controller.GetComponent<LoadingArmController>();
            scriptActual = script;
            if (script.isClamped)
            {
                script.enableERC = true;
                hasEntered = true;
            }
		}
	}
}
