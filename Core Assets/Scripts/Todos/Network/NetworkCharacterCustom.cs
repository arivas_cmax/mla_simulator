﻿using UnityEngine;
using System.Collections;

public class NetworkCharacterCustom : Photon.MonoBehaviour {

	Vector3 realPosition = Vector3.zero;
	Quaternion realRotation = Quaternion.identity;

    public bool startPositionHere = false;

    public bool startPositionCustom = false;
    public Transform customPosition;

	float lastUpdateTime;

    public bool hideMine = false;
    public Material transparentMaterial;

	// Use this for initialization
	void Start () {
        if (startPositionHere)
        {
            realPosition = transform.position;
            realRotation = transform.rotation;

            transform.position = realPosition;
            transform.rotation = realRotation;
        }

        if (startPositionCustom)
        {
            Debug.Log("Starting position as a MAIN player");
            realPosition = customPosition.position;
            realRotation = customPosition.rotation;

            transform.position = realPosition;
            transform.rotation = realRotation;
        } 
	}

	// Update is called once per frame
	void Update () {
		if (photonView.isMine) {
			//nothing, dudie
            if (hideMine)
            {
                GetComponent<Renderer>().material = transparentMaterial;
            }
		} else {
			transform.position = Vector3.Lerp(transform.position, realPosition, 0.1f);
			transform.rotation = Quaternion.Lerp(transform.rotation, realRotation, 0.1f);
		}
	}
	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting) {
			//Our player
			stream.SendNext (transform.position);
			stream.SendNext (transform.rotation);
		} else {
			//Other players
			realPosition = (Vector3)stream.ReceiveNext();
			realRotation = (Quaternion)stream.ReceiveNext();
		}
	}
}
