﻿using UnityEngine;
using System.Collections;

public class Gizmo : MonoBehaviour 
{
    public float pivoteSize = .75f;
    public Color pivoteColor = Color.yellow;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	


	}

    void OnDrawGizmos()
    {
        Gizmos.color = pivoteColor;
        Gizmos.DrawWireSphere(transform.position, pivoteSize);        

    }
}
