﻿using UnityEngine;
using System.Collections;

public class SMStart : StateBase {

    public SMStart()
    {
        nextStateYes = new SMSwitchOn();
    }

    public override bool execute()
    {
        bool idle = true;
        while (idle)
        {
            if (getStateParameters().IsStarted)
                idle = false;
        }

        Debug.Log("System started");

        return true;
    }

}
