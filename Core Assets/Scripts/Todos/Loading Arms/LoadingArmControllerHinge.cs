﻿using UnityEngine;
using System.Collections;

public class LoadingArmControllerHinge : MonoBehaviour {

	public GameObject rotorPart;
	public GameObject upDownPart;
	public GameObject shoreShipPart;

	int control = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey (KeyCode.RightArrow)) {
			Debug.Log ("Right");

			if(control == 0)
				moveArmPart(upDownPart, true);
			else if (control == 1)
				moveArmPart(shoreShipPart, true);
			else if (control == 2)
				rotateArmPart(rotorPart, true);
		}
		
		if (Input.GetKey (KeyCode.LeftArrow)) {
			Debug.Log ("Left");

			if(control == 0)
				moveArmPart(upDownPart, false);
			else if (control == 1)
				moveArmPart(shoreShipPart, false);
			else if (control == 2)
				rotateArmPart(rotorPart, false);
		}

		if (Input.GetKeyUp (KeyCode.RightArrow)) {
			Debug.Log ("Right");
			
			if(control == 0)
				stopArmPart(upDownPart);
			else if (control == 1)
				stopArmPart(shoreShipPart);
		}
		
		if (Input.GetKeyUp (KeyCode.LeftArrow)) {
			Debug.Log ("Left");
			
			if(control == 0)
				stopArmPart(upDownPart);
			else if (control == 1)
				stopArmPart(shoreShipPart);
		}

		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			if (control >= 2)
				control = 0;
			else
				control++;

			Debug.Log ("Up: " + control);
		}
		
		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			if (control <= 0)
				control = 2;
			else
				control--;

			Debug.Log ("Down: " + control);
		}
		
	}

	void rotateArmPart(GameObject part, bool direction){
		int directionNumber = 1;
		if (direction) {
			directionNumber = 1;
		}else{
			directionNumber = -1;
		}

		part.transform.Rotate(-Vector3.up * 30 * Time.deltaTime * directionNumber);
	}

	void moveArmPart(GameObject part, bool direction){
		int directionNumber = 1;
		if (direction) {
			directionNumber = 1;
		}else{
			directionNumber = -1;
		}

		//part.GetComponent<Rigidbody> ().drag = 0;
		//part.GetComponent<Rigidbody> ().angularDrag = 0;
		
		HingeJoint hinge = (HingeJoint)part.GetComponent ("HingeJoint");
		
		JointMotor motor = hinge.motor;
		
		motor.force = 1500;
		motor.targetVelocity = 15 * directionNumber;
		motor.freeSpin = false;

		hinge.motor = motor;
		
	}

	void stopArmPart(GameObject part){
		HingeJoint hinge = (HingeJoint)part.GetComponent ("HingeJoint");
		
		JointMotor motor = hinge.motor;
		
		motor.force = 0;
		motor.targetVelocity = 0;
		motor.freeSpin = false;

		//part.GetComponent<Rigidbody> ().drag = 1500;
		//part.GetComponent<Rigidbody> ().angularDrag = 1500;

		hinge.motor = motor;
	}
}
