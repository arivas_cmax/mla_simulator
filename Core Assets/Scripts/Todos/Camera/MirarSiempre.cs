﻿using UnityEngine;
using System.Collections;

public class MirarSiempre : MonoBehaviour {

    public GameObject objetivo;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(objetivo.transform.position);
	}
}
