﻿using UnityEngine;
using System.Collections;

public class LocalArmSelector : MonoBehaviour {

	public GameObject controller;
	public GameObject rotor;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		LoadingArmSelector script = controller.GetComponent<LoadingArmSelector>();
		script.nextArm();
		rotor.transform.Rotate(new Vector3(0,0,45));
	}
}
