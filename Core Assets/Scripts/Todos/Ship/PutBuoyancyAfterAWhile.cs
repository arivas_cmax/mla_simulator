﻿using UnityEngine;
using System.Collections;

public class PutBuoyancyAfterAWhile : MonoBehaviour {

	public float waitTime = 1f;
	public GameObject floatingObject;
	
	// Use this for initialization
	void Start () {
		StartCoroutine(putInsideFloatingObject());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	IEnumerator putInsideFloatingObject(){
		yield return new WaitForSeconds(waitTime);
		gameObject.transform.parent = floatingObject.transform;
	}
}
