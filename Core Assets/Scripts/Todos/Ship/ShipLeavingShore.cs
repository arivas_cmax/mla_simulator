﻿using UnityEngine;
using System.Collections;

public class ShipLeavingShore : MonoBehaviour {

	public float force;

	private bool isActive = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("ShipAway")) {
            if (StateParameters.getInstance().IsDevMode)
            {
                Debug.Log("Ship getting away");
                isActive = true;

                TritonUnity triton = (TritonUnity)(GameObject.FindObjectOfType(typeof(TritonUnity)));
                //triton.GetOcean().SetRefractionColor(new Triton.Vector3(1, 0, 0));
                triton.windSpeed += 4;
            }
		}
	}

	void FixedUpdate () {
		if(isActive){
			GetComponent<Rigidbody>().AddForce (-force, 0, 0);

            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationY;
		}
	}
}
