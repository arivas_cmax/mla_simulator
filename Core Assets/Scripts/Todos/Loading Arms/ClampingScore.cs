﻿using UnityEngine;
using System.Collections;

public class ClampingScore : MonoBehaviour {

    public bool hasEntered = false;

    public GameObject armPoint;

    public float score;
    public float distanceX;
    public float distanceY;
    public float distanceZ;

	// Use this for initialization
	void Start () {
        hasEntered = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        hasEntered = true;
        armPoint = other.gameObject;
    }

    void OnTriggerExit(Collider other)
    {
        hasEntered = false;
        armPoint = null;
    }

    public float putScore()
    {
        if(armPoint != null)
        {
            score = Vector3.Distance(gameObject.transform.position, armPoint.transform.position);

            distanceX = Mathf.Abs(gameObject.transform.position.x - armPoint.transform.position.x);
            distanceY = Mathf.Abs(gameObject.transform.position.y - armPoint.transform.position.y);
            distanceZ = Mathf.Abs(gameObject.transform.position.z - armPoint.transform.position.z);

            return score;
        }
        else
        {
            return 0;
        }
    }
}
