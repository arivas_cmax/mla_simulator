﻿using UnityEngine;
using System.Collections;

public class RotatingBeaconAlarm : MonoBehaviour {

    public static bool isActiveAlarm = false;
    public AudioSource alarm;
    public float rotationSpeed = 1;

    public GameObject alarmLightAndSound;

	// Use this for initialization
	void Start () {
        isActiveAlarm = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (isActiveAlarm)
        {
            transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed);
            alarmLightAndSound.SetActive(true);
        }
        else
        {            
            alarmLightAndSound.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            isActiveAlarm = !isActiveAlarm;
        }

        if(TriggerERC.hasEntered)
        {
            isActiveAlarm = true;
        }
	}
}
