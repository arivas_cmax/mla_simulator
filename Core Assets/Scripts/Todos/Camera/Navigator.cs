﻿using UnityEngine;
using System.Collections;

public class Navigator : MonoBehaviour {

	public GameObject target;
	public GameObject camera;

	public bool isInside = false;
	public Transform position;

	private bool isMoving = false;

	public float travelTime = 2f;

	public float lockTimeToTravel = 3f;
	public float lockTime = 0;

	public GameObject barraEstado;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if(!isInside)
		{
			Ray ray = new Ray(gameObject.transform.position, gameObject.transform.forward);
			RaycastHit hit = new RaycastHit();
			LayerMask layerMask = 1 << 12;

			if (Physics.Raycast(ray, out hit, 5000, layerMask))
			{
				Debug.DrawRay(gameObject.transform.position, gameObject.transform.forward, Color.cyan);
				if(!isMoving)
					target.transform.position = hit.point;
			}
			else
			{
				Debug.DrawRay(gameObject.transform.position, gameObject.transform.forward, Color.cyan);
			}

			barraEstado.SetActive(false);
		}
		else
		{
			if(lockTime >= lockTimeToTravel)
			{
				camera.gameObject.transform.parent = position;
				
				iTween.MoveTo(camera, iTween.Hash("position", new Vector3(0,0,0), "easetype", iTween.EaseType.easeInOutQuad, "time", travelTime, "islocal", true));
				isMoving = true;
				StartCoroutine(rotateCamera(camera));
				barraEstado.SetActive(false);
			}
			else
			{
				if(!isMoving)
				{
					barraEstado.SetActive(true);
					Vector3 escala = barraEstado.transform.localScale;
					escala.x = lockTime / lockTimeToTravel;
					barraEstado.transform.localScale = escala;
				}
			}

			lockTime += Time.deltaTime;


		}
	}

	IEnumerator rotateCamera(GameObject movableCamera){
		yield return new WaitForSeconds(travelTime);
		//iTween.RotateTo (movableCamera.gameObject, iTween.Hash("easetype", iTween.EaseType.easeInOutQuad, "x", 0, "y", 0, "z", 0, "time", travelTime, "islocal", true));
		isMoving = false;
		lockTime = 0;
	}
}
