﻿using UnityEngine;
using System.Collections;

public class CameraControlOculus : MonoBehaviour
{

    public GameObject primaryCamera;


    public Camera mainCamera;
    public Camera secondaryCamera;
    public GameObject[] cameraPlacements;

    public float movementSensitivity;

    int actualMain = 0;
    int actualSecondary = 0;

    public float travelTime = 2f;
    private bool isMoving = false;

    public GameObject ghost1;
    public GameObject ghost2;
    public bool ghostActive = false;

    bool isTimeShift = false;

    public bool isHalpsVisible = true;
    public GameObject halps;

    private GameObject movable;

    private bool PlayerOrMLA; // False = Player / True = MLA

    void Start()
    {
        movable = gameObject;
    }
    // Update is called once per frame
    void Update()
    {
        getMovement();

    }

    private void getMovement()
    {
        if (Input.GetButton("NextCamera") || Input.GetButton("PreviousCamera"))
        {
            movable = primaryCamera;
        }

        if (Input.GetButton("NextSecondaryCamera"))
        {
            moveGameObject(2, true);
        }
        if (Input.GetButton("PreviousSecondaryCamera"))
        {
            moveGameObject(2, false);
        }
        if (!Input.GetButton("PlayerOrMLA"))
        {
            PlayerOrMLA = false;
        }
        else
        {
            PlayerOrMLA = true;
        }
        if (!PlayerOrMLA)
        {
            if (Input.GetAxis("Horizontal") < 0)
            {              
                moveGameObject(3, false);
            }
            if (Input.GetAxis("Horizontal") > 0)
            {
                moveGameObject(3, true); 
            }
            if (Input.GetAxis("Vertical") > 0)
            {
                moveGameObject(1, false);
            }
            if (Input.GetAxis("Vertical") < 0)
            {
                moveGameObject(1, true);               
            }
        }        
    }
    private void moveGameObject(int axis, bool positivo)
    {
        int direction = 1;
        if (!positivo)
        {
            direction = -1;
        }
        switch (axis)
        {
            case 1:
                movable.transform.Translate(Vector3.back * movementSensitivity * Time.deltaTime * direction, secondaryCamera.transform);
                break;

            case 2:
                movable.transform.Translate(Vector3.up * movementSensitivity * Time.deltaTime * direction);
                break;

            case 3:
                movable.transform.Translate(Vector3.right * movementSensitivity * Time.deltaTime * direction, secondaryCamera.transform);
                break;
        }
    }
}
