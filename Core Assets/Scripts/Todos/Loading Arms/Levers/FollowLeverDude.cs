﻿using UnityEngine;
using System.Collections;

public class FollowLeverDude : MonoBehaviour {

    public GameObject leverReference;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = leverReference.transform.position;
	}
}
