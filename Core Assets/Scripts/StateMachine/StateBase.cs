﻿
public abstract class StateBase : IState {
    public IState nextStateYes { get; set; }
    public IState nextStateNo { get; set; }
    public bool PresetToFail { get; set; }

    public StateParameters getStateParameters()
    {
        return StateParameters.getInstance();
    }

    public abstract bool execute();
}
