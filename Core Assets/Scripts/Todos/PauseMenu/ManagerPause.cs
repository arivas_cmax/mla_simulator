﻿using UnityEngine;
using System.Collections;

public class ManagerPause : MonoBehaviour 
{
	public GameObject Panel;

	public void closeApp()
	{
		Application.Quit();
	}

	public void ChangeScene(string scene)
	{
        Time.timeScale = 1;
        AudioListener.volume = 1;
		Application.LoadLevel(scene);
	}

	public void UnPauseSimulation()
	{
		Panel.SetActive(false);
		Time.timeScale = 1;
		AudioListener.volume = 1;
	}
}