﻿using UnityEngine;
using System.Collections;

public class DummyCode : MonoBehaviour
{

    public Camera mainCamera;
    public Camera secondaryCamera;
    public GameObject[] cameraPlacements;

    public float movementSensitivity;

    int actualMain = 0;
    int actualSecondary = 0;

    public float travelTime = 2f;
    private bool isMoving = false;

    public GameObject ghost1;
    public GameObject ghost2;
    public bool ghostActive = false;

    bool isTimeShift = false;

    public bool isHalpsVisible = true;
    public GameObject halps;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //MovimientoCamara
        if (Input.GetButton("NextSecondaryCamera"))
        {
            moveCamera(mainCamera, 2, true);
        }
        if (Input.GetButton("PreviousSecondaryCamera"))
        {
            moveCamera(mainCamera, 2, false);
        }
        if (Input.GetAxis("PadH") < 0)
        {
            moveCamera(mainCamera, 1, true);
        }
        if (Input.GetAxis("PadH") > 0)
        {
            moveCamera(mainCamera, 1, false);
        }
        if (Input.GetAxis("PadV") > 0)
        {
            moveCamera(mainCamera, 3, true);
        }
        if (Input.GetAxis("PadV") < 0)
        {
            moveCamera(mainCamera, 3, false);
        }

        ghost1.transform.position = secondaryCamera.gameObject.transform.position;

        ghost1.transform.rotation = secondaryCamera.gameObject.transform.rotation;
    }

    private void moveCamera(Camera movableCamera, int axis, bool positivo)
    {
        //Debug.Log ("Moviendo Camara");

        Vector3 newTransform = movableCamera.gameObject.transform.localPosition;

        int direction = 1;
        if (!positivo)
            direction = -1;

        if (axis == 1)
        {
            //newTransform += movementSensitivity * Time.deltaTime * direction;
            gameObject.transform.Translate(Vector3.back * movementSensitivity * Time.deltaTime * direction, secondaryCamera.transform);
        }
        else if (axis == 2)
        {
            gameObject.transform.Translate(Vector3.up * movementSensitivity * Time.deltaTime * direction);
        }
        else if (axis == 3)
        {
            //newTransform.z += movementSensitivity * Time.deltaTime * direction;
            gameObject.transform.Translate(Vector3.right * movementSensitivity * Time.deltaTime * direction, secondaryCamera.transform);
        }

        //movableCamera.gameObject.transform.localPosition = newTransform;
    }
}
