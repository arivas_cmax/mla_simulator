﻿using UnityEngine;
using System.Collections;

public class PutColliderInPlace : MonoBehaviour {

	public GameObject reference;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = reference.transform.position;
		transform.rotation = reference.transform.rotation;
	}
}
