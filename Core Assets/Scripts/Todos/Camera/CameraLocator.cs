﻿using UnityEngine;
using System.Collections;

public class CameraLocator : MonoBehaviour {

    public GameObject player;

	public GameObject mainCamera;
    public GameObject secondaryCamera;
	public GameObject[] cameraPlacements;

	public float movementSensitivity;

	int actualMain = 0;
	int actualSecondary = 0;

	public float travelTime = 2f;
	private bool isMoving = false;

	public GameObject ghost1;
	public GameObject ghost2;
	public bool ghostActive = false;

    bool isTimeShift = false;

    public bool isHalpsVisible = true;
    public GameObject halps;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.D))
        {
            StateParameters.getInstance().IsDevMode = !StateParameters.getInstance().IsDevMode;
        }

        if (Input.GetButtonDown("Help"))
        {
            showHelp();
        }

        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            Application.LoadLevel(Application.loadedLevelName);
        }

        
          if (!isMoving)
            {
                if (Input.GetButtonDown("NextCamera"))
                {
                    changeCamera(mainCamera, ref actualMain, true);
                }
                if (Input.GetButtonDown("PreviousCamera"))
                {
                    changeCamera(mainCamera, ref actualMain, false);
                }
            }
       

        if (StateParameters.getInstance().IsDevMode)
        {
            if (Input.GetButton("ShowGhosts"))
            {
                ghostActive = !ghostActive;
                ghost1.SetActive(ghostActive);
                ghost2.SetActive(ghostActive);
            }            

            if ((Input.GetKeyDown(KeyCode.T)) || Input.GetButtonDown("TimeShift"))
            {
                timeShift();
            }
        }
	}

    void showHelp()
    {
        isHalpsVisible = !isHalpsVisible;
        halps.SetActive(isHalpsVisible);
    }

    void timeShift()
    {
        if (isTimeShift)
        {
            Time.timeScale = 1;
        }
        else
        {
            Time.timeScale = 5;
        }

        isTimeShift = !isTimeShift;
    }

	private void moveCamera(GameObject movableCamera, int axis, bool positivo){
		//Debug.Log ("Moviendo Camara");

		Vector3 newTransform = movableCamera.gameObject.transform.localPosition;

		int direction = 1;
		if(!positivo)
			direction = -1;

		if(axis == 1){
			//newTransform += movementSensitivity * Time.deltaTime * direction;
            movableCamera.transform.Translate(Vector3.back * movementSensitivity * Time.deltaTime * direction, secondaryCamera.transform);
		}else if (axis == 2) {
            movableCamera.transform.Translate(Vector3.up * movementSensitivity * Time.deltaTime * direction);
		}else if (axis == 3) {
			//newTransform.z += movementSensitivity * Time.deltaTime * direction;
            movableCamera.transform.Translate(Vector3.right * movementSensitivity * Time.deltaTime * direction, secondaryCamera.transform);
		}

		//movableCamera.gameObject.transform.localPosition = newTransform;
	}

    private void changeCamera(GameObject movableCamera, ref int actual, bool next)
    {

		if(next)
		{
			if(actual >= cameraPlacements.Length - 1)
			{
				actual = 0;
			}else{
				actual++;
			}
		}else{
            
			if(actual <= 0)
			{
				actual = cameraPlacements.Length - 1;
			}else{
				actual--;
			}
		}

		movableCamera.gameObject.transform.parent = cameraPlacements[actual].transform;

		iTween.MoveTo(movableCamera.gameObject, iTween.Hash("position", new Vector3(0,0,0), "easetype", iTween.EaseType.easeInOutQuad, "time", travelTime, "islocal", true));
		isMoving = true;
		StartCoroutine(rotateCamera(movableCamera));
	}

    IEnumerator rotateCamera(GameObject movableCamera)
    {
		yield return new WaitForSeconds(travelTime);
		//iTween.RotateTo (movableCamera.gameObject, iTween.Hash("easetype", iTween.EaseType.easeInOutQuad, "x", 0, "y", 0, "z", 0, "time", travelTime, "islocal", true));
		isMoving = false;
	}
}
