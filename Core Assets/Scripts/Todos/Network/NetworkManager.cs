﻿using UnityEngine;
using System.Collections;

public class NetworkManager : MonoBehaviour {

    public GameObject IdleCamera;
    public GameObject rightEyeCamera;

    public GameObject[] armControllers;

    public Transform spawnPointPrimary;

    public GameObject localPlayerPrefab;

	// Use this for initialization
	void Start () {
        
		Connect ();
	}

	void Connect(){
		PhotonNetwork.ConnectUsingSettings ("Pruebas v001");
	}

    private void OnFailedToConnectToPhoton(object parameters)
    {
        PhotonNetwork.offlineMode = true;
        SpawnPlayerLocal();
        Debug.Log("OnFailedToConnectToPhoton. StatusCode: " + parameters);
    }

	void OnGUI () {
		GUILayout.Label ( PhotonNetwork.connectionStateDetailed.ToString() );
	}

	void OnJoinedLobby(){
		PhotonNetwork.JoinRandomRoom ();
	}

	void OnPhotonRandomJoinFailed() {
		PhotonNetwork.CreateRoom (null);
	}

	void OnJoinedRoom(){
		SpawnPlayer ();
	}

	void SpawnPlayer(){

        int playerCount = PhotonNetwork.playerList.Length;
        Debug.Log("Player count: " + playerCount);
        if(playerCount > 1)
        {
            foreach (GameObject armController in armControllers)
            {
                LoadingArmController loadingArmController = armController.GetComponent<LoadingArmController>();
                loadingArmController.isFreeWheel = true;
                loadingArmController.isActiveArm = false;
                loadingArmController.remotelyControlled = true;
            }
        }

		GameObject mySelf = (GameObject) PhotonNetwork.Instantiate ("PlayerCube", Vector3.zero, Quaternion.identity, 0);
		//GameObject mySelf = (GameObject) PhotonNetwork.Instantiate ("PlayerFPS", Vector3.zero, Quaternion.identity, 0);

        IdleCamera.transform.position = mySelf.transform.position;
        IdleCamera.transform.rotation = mySelf.transform.rotation;
        IdleCamera.transform.parent = mySelf.transform;

        mySelf.GetComponent<NetworkCharacterCustom>().customPosition = spawnPointPrimary;
        mySelf.GetComponent<NetworkCharacterCustom>().startPositionCustom = true;

        mySelf.GetComponent<CameraControlOculus>().enabled = true;
        mySelf.GetComponent<CameraControlOculus>().primaryCamera = IdleCamera;
        mySelf.GetComponent<CameraControlOculus>().mainCamera = IdleCamera.GetComponent<Camera>();
        mySelf.GetComponent<CameraControlOculus>().secondaryCamera = rightEyeCamera.GetComponent<Camera>();

		//mySelf.GetComponent<AudioSource> ().enabled = true;
		//mySelf.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController> ().enabled = true;
		//mySelf.GetComponentInChildren<Camera> ().enabled = true;
		//mySelf.GetComponent<CharacterController> ().enabled = true;
		//mySelf.GetComponent<AudioSource> ().enabled = true;
	}

    void SpawnPlayerLocal()
    {
        GameObject mySelf = (GameObject)Instantiate(localPlayerPrefab);
        //GameObject mySelf = (GameObject) PhotonNetwork.Instantiate ("PlayerFPS", Vector3.zero, Quaternion.identity, 0);

        IdleCamera.transform.position = mySelf.transform.position;
        IdleCamera.transform.rotation = mySelf.transform.rotation;
        IdleCamera.transform.parent = mySelf.transform;

        mySelf.GetComponent<NetworkCharacterCustom>().customPosition = spawnPointPrimary;
        mySelf.GetComponent<NetworkCharacterCustom>().startPositionCustom = true;

        mySelf.GetComponent<CameraControlOculus>().enabled = true;
        mySelf.GetComponent<CameraControlOculus>().mainCamera = IdleCamera.GetComponent<Camera>();
        mySelf.GetComponent<CameraControlOculus>().secondaryCamera = rightEyeCamera.GetComponent<Camera>();

        //mySelf.GetComponent<AudioSource> ().enabled = true;
        //mySelf.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController> ().enabled = true;
        //mySelf.GetComponentInChildren<Camera> ().enabled = true;
        //mySelf.GetComponent<CharacterController> ().enabled = true;
        //mySelf.GetComponent<AudioSource> ().enabled = true;
    }

    private void initializeCameraLocator()
    {

    }
}
