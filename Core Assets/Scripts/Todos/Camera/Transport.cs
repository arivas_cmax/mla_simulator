﻿using UnityEngine;
using System.Collections;

public class Transport : MonoBehaviour {

	public GameObject navigator;

	public bool playerInside = false;

	public Material materialTransparente;
	public Material materialFocus;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		if(other.tag == "PlayerCamera")
		{
			Debug.Log("Player entered the thing");
			playerInside = true;
		}

		if(other.tag == "Navigator")
		{
			if(!playerInside)
			{
				gameObject.GetComponent<Renderer>().material = materialFocus;
				Navigator nahvii = navigator.GetComponent<Navigator>();
				nahvii.isInside = true;
				nahvii.position = transform;
			}
		}
	}
	
	void OnTriggerExit(Collider other){
		if(other.tag == "PlayerCamera")
		{
			Debug.Log("Player exited the thing");
			playerInside = false;
		}

		if(other.tag == "Navigator")
		{
			gameObject.GetComponent<Renderer>().material = materialTransparente;
			Navigator nahvii = navigator.GetComponent<Navigator>();
			nahvii.isInside = false;
			nahvii.position = null;
		}
	}
}
