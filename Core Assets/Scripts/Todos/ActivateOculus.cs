﻿using UnityEngine;
using System.Collections;

public class ActivateOculus : MonoBehaviour
{
    private static bool oculusActivo = false;

    public GameObject oculusCamera;
    public Camera mainCamera;

    public GameObject tritonSea;
    //public GameObject tasharenWater;

    // Use this for initialization
    void Start()
    {
        /*if (oculusActivo)
        {
            Debug.Log("Activando oculo");
            //tasharenWater.SetActive(true);
            mainCamera.enabled = false;
            oculusCamera.SetActive(true);            
        }
        else
        {
            Debug.Log("Desactivando oculo");
            tasharenWater.SetActive(false);
            oculusCamera.gameObject.SetActive(false);
            mainCamera.enabled = true;
        }*/

        activateOculus();
    }

    // Update is called once per frame
  /*  void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
        //    Application.LoadLevel(Application.loadedLevel);
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
        //    activateOculus();
        }
    }
    */
    public void activateOculus()
    {
        Debug.Log("Activando oculo");
        mainCamera.enabled = false;
        oculusCamera.SetActive(true);
        //tasharenWater.SetActive(true);
    }
}
