﻿using UnityEngine;
using System.Collections;

public class StateMachine {

    private static StateMachine instance = null;

    public IState state = null;

    public StateParameters StateMachineParameters { get; set; }

	public string mensaje = "Singleton funcionando correctamente.";    

    private StateMachine()
    {

	}

    public static StateMachine getInstance()
    {
		if(instance == null)
		{
            instance = new StateMachine();

            instance.StateMachineParameters = StateParameters.getInstance();

			return instance; 
		}else{
			return instance; 
		}
	}

    public void start()
    {
        state = new SMStart();
        executeState();
    }

    private void executeState()
    {
        if (state.execute())
            state = state.nextStateYes;
        else
            state = state.nextStateNo;

        executeState();
    }    
}
