﻿using UnityEngine;
using System.Collections;

public class LocalSlowButton : MonoBehaviour {

	public GameObject controller;
	private bool itsFirstTime = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		if((other.tag == "Player") && (itsFirstTime))
		{
			Debug.Log("Eslowly");
			LoadingArmSelector scriptSelector = controller.GetComponent<LoadingArmSelector>();
			LoadingArmController scriptController = scriptSelector.getCurrentArm().GetComponent<LoadingArmController>();
			scriptController.isFasterThanChiktikkaFastpaws = !scriptController.isFasterThanChiktikkaFastpaws;

			itsFirstTime = false;
		}
	}
	
	void OnTriggerExit(Collider other){
		if(other.tag == "Player")
		{
			itsFirstTime = true;
		}
	}
}
