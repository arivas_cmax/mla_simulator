﻿using UnityEngine;
using System.Collections;

public class Handler : MonoBehaviour {

    public void closeApp()
    {
        Application.Quit();
    }

    public void ChangeScene(string scene)
    {
        Application.LoadLevel(scene);
    }
}
