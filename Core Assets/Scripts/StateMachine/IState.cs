﻿using UnityEngine;
using System.Collections;

public interface IState {

    IState nextStateYes { get; set; }
    IState nextStateNo { get; set; }
    bool PresetToFail { get; set; }
    bool execute();
}
