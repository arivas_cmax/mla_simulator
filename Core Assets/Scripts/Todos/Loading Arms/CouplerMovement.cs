﻿using UnityEngine;
using System.Collections;

public class CouplerMovement : MonoBehaviour {

	Quaternion posicionVertical;

	// Use this for initialization
	void Start () {
		posicionVertical = transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {

		Quaternion posicionNueva = transform.rotation;

		posicionNueva.y = posicionVertical.y;

		transform.rotation = posicionNueva;
	}
}
