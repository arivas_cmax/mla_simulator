﻿using UnityEngine;
using System.Collections;

public class Pantograph : MonoBehaviour {

	public GameObject positionReference;
	public GameObject rotationReference;

	/*public float radius;

	public float centerZ;
	public float centerY;*/

	private float startingX;
	private float startingY;
	private float startingZ;

	// Use this for initialization
	void Start () {
		startingX = transform.localPosition.x;
		startingY = transform.localPosition.y;
		startingZ = transform.localPosition.z;
	}
	
	// Update is called once per frame
	void Update () {
		/*float angle = reference.transform.localRotation.x;

		float newZ = centerZ + (radius * Mathf.Cos (angle));
		float newY = centerY + (radius * Mathf.Sin (angle));

		transform.localPosition = new Vector3 (startingX, -newY + startingY, newZ);*/

		//Debug.Log (positionReference.transform.position);
		transform.position = positionReference.transform.position;
		transform.rotation = rotationReference.transform.rotation;
	}
}
