﻿using UnityEngine;
using System.Collections;

public class MoveLever : MonoBehaviour {

    public int leverID;

    //public Quaternion moveTo;
    private Quaternion startRotation;

    public bool isRight = true;
    private int leftRightFactor = 1;

    private float tiempo = 0;

    bool isRotatoreable = false;
    bool isGoingDown = true;
    bool isReadyForRotation = true;

    public GameObject otherLever;

	// Use this for initialization
	void Start () {
        startRotation = transform.rotation;
        tiempo = 0;
        isGoingDown = true;

        if (!isRight)
            leftRightFactor = -1;
	}
	
	// Update is called once per frame
	void Update () {
        if (isRotatoreable)
        {
            int grade = 1;
            if (!isGoingDown)
                grade = -1;

            transform.Rotate(leftRightFactor * grade * 50 * Time.deltaTime, 0, 0);

            tiempo += Time.deltaTime;
            if (tiempo >= 2)
            {
                isRotatoreable = false;
                tiempo = 0;
                isReadyForRotation = true;
            }
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if ((other.tag == "Player") && (isReadyForRotation))
        {
            triggerLever();

            otherLever.GetComponent<MoveLever>().triggerLever();
        }
    }

    public void triggerLever()
    {
        StateParameters armState = StateParameters.getInstance();
        switch (leverID)
        {
            case 1:
                armState.MechanicalLockLeft = isGoingDown;
                break;
            case 2:
                armState.MechanicalLockRight = isGoingDown;
                break;
            case 3:
                armState.HydraulicLock = isGoingDown;
                break;
            case 4:
                armState.ErcArmed = isGoingDown;
                break;
            default:
                Debug.Log("Switch not configured");
                break;
        }

        isRotatoreable = true;
        isGoingDown = !isGoingDown;
        isReadyForRotation = false;
    }
}
