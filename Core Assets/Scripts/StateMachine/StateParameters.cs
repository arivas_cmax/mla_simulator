﻿using UnityEngine;
using System.Collections;

public class StateParameters {

    private static StateParameters instance = null;

	public string mensaje = "Singleton funcionando correctamente.";

    public bool IsStarted { get; set; }
    public bool IsOn { get; set; }
    public bool MainIsolator { get; set; }
    public bool PlcStarted { get; set; }
    public bool HornSound { get; set; }
    public bool AlarmAcknowledged { get; set; }
    public bool ErcAlarmButton { get; set; }
    public bool PreAlarmButton { get; set; }
    public bool PowerButton { get; set; }
    public bool PowerLight { get; set; }
    public bool Beacon { get; set; }
    public bool OilLevelAlarm { get; set; }
    public bool OilPressureAlarm { get; set; }
    public bool CommonFuseAlarm { get; set; }
    public bool ControllerSelected { get; set; }
    public bool ArmSelected { get; set; }
    public bool ManeuveringModeSelected { get; set; }
    public bool MechanicalLockLeft { get; set; }
    public bool MechanicalLockRight { get; set; }
    public bool HydraulicLock { get; set; }
    public bool ArmUnlockedLight { get; set; }
    public bool IsMoving { get; set; }
    public bool BlindPlateRemoved { get; set; }
    public bool CouplerClosing { get; set; }
    public bool CouplerClosed { get; set; }
    public bool SupportJAckAdjusted { get; set; }
    public bool FreeWheelState { get; set; }
    public bool ManeuveringAlarm { get; set; }
    public bool CouplerClosedLight { get; set; }
    public bool UpperErcValveOpen { get; set; }
    public bool UpperErcValveOpening { get; set; }
    public bool ErcArmed { get; set; }
    public bool ErcDisarmedAlarm { get; set; }
    public bool PermissionToLoad { get; set; }
    public bool IsLoading { get; set; }
    public bool EmergencySituation { get; set; }
    public bool AutomaticPrealarm { get; set; }
    public bool ManualPrealarm { get; set; }
    public bool ArmWithinEnvelope { get; set; }
    public bool EsdSignal { get; set; }
    public bool AutomaticErcAlarm { get; set; }
    public bool ManualErcAlarm { get; set; }
    public bool ManualErcButton { get; set; }
    public bool HardwiredErcDcs { get; set; }
    public bool ErcActivated { get; set; }
    public bool IsSafeCondition { get; set; }

    public bool IsDevMode { get; set; }


    private StateParameters()
    {
		//TODO: Colocar maquina de estado
	}

    public static StateParameters getInstance()
    {
		if(instance == null)
		{
            instance = new StateParameters();

			return instance; 
		}else{
			return instance; 
		}
	}

}
