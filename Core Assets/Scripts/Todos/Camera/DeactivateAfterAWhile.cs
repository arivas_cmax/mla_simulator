﻿using UnityEngine;
using System.Collections;

public class DeactivateAfterAWhile : MonoBehaviour {

	public float deactivationTime = 1f;

	// Use this for initialization
	void Start () {
		StartCoroutine(deactivateNow());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator deactivateNow(){
		yield return new WaitForSeconds(deactivationTime);
		gameObject.GetComponent<Camera>().enabled = false;
	}
}
