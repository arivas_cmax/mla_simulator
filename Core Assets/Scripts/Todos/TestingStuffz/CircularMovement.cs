﻿using UnityEngine;
using System.Collections;

public class CircularMovement : MonoBehaviour {

	public GameObject cameraPlacement;
	public float radius;
	public float height;

	// Use this for initialization
	void Start () {
		Vector3 nuevaPosicion = new Vector3(transform.position.x + radius, transform.position.y + height, transform.position.z);

		cameraPlacement.transform.position = nuevaPosicion;
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(Vector3.up * Time.deltaTime);
	}
}
