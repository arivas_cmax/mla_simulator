﻿using UnityEngine;
using System.Collections;

public class videoMenu : MonoBehaviour
{
    public GameObject Panel;
    public Animator animatorcontroller;
    public GameObject Ancla;

    public void ActivaAnimacion()
    {        
        if (Panel.activeSelf)
        {            
            animatorcontroller.Play("VideoFadeOut");
            Ancla.active = true;            
        }
        else
        {            
            animatorcontroller.Play("VideoFadeIn");
            Ancla.active = false;
        }
    }
}