﻿using UnityEngine;
using System.Collections;

public class PistonMovement : MonoBehaviour {

	public GameObject embol;
	public GameObject jointConnection;
	public float angleAdjustmentX;
	public float angleAdjustmentY;
	public float angleAdjustmentZ;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt (jointConnection.transform.position);
		transform.rotation *= Quaternion.Euler(angleAdjustmentX, angleAdjustmentY, angleAdjustmentZ);
		embol.transform.position = jointConnection.transform.position;
	}
}
