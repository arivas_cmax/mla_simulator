﻿using UnityEngine;
using System.Collections;

public class LoadingArmController : MonoBehaviour
{
    private bool blockCouplerAnimation = false;
    private float couplerDelay = 3f;

    public bool isActiveArm;

    public GameObject rotorPart;
    public GameObject upDownPart;
    public GameObject shoreShipPart;
    public GameObject couplerClamps;

    public GameObject ERCTop;
    public GameObject ERCBottom;
    public GameObject ERCTopContainer;
    public GameObject ERCBottomContainer;

    public GameObject vessel;
    public GameObject vesselFloatingContainer;

    public GameObject limitAlarmGreen;
    public GameObject limitAlarmYellow;
    public GameObject limitAlarmRed;

    public GameObject[] limitAlarmsRed;
    public Material alarmGreen;
    public Material alarmYellow;
    public Material alarmRed;
    public Material alarmTransparent;

    private bool isActiveLimitAlarm = false;

    public float upDownPartRotation = 0;
    public float shoreShipPartRotation = 0;
    public float targetAngle = 0;

    int control = 0;

    public float rotateSpeed = 5f;

    private float actualSpeed;

    private bool isRotating = false;

    public bool isClamped = false;

    public bool isFreeWheel = false;

    public GameObject[] clampColliders;
    public GameObject[] allColliders;
    public GameObject[] clampSupportColliders;
    public GameObject hoseCollidersContainer;

    public float[] limitsArm1;
    public float[] limitsArm2;
    public float[] limitsSwivel;

    public float[] limitsERC;

    public GameObject jointForFreeWheel;

    private bool isERCActivated = false;

    private float[] limitsShoreShipERC;
    private float[] limitsUpDownERC;

    private string proportioner = "";

    public bool enableERC = false;
    private bool isJustOnceERC = false;

    private bool isFillingVessel = false;

    public GameObject halps;
    private bool isHalpsVisible = true;

    //Buttons pressed, my friend, yes....
    public bool isMovingShip = false;
    public bool isMovingShore = false;
    public bool isMovingUp = false;
    public bool isMovingDown = false;
    public bool isMovingLeft = false;
    public bool isMovingRight = false;

    public bool isFasterThanChiktikkaFastpaws = true;

    public bool doCoupling = false;

    public GameObject[] clampingScoreColliders;

    public TextMesh scoreText;

    public bool autoClamp = false;
    public GameObject puntaAcople;
    public GameObject objective;

    private bool automaticXAccomplished = false;
    private bool automaticYAccomplished = false;
    private bool automaticZAccomplished = false;

    private float contadorTiempo = 0;
    private bool contadorTiempoIniciado = false;

    public bool remotelyControlled = false;

    // Use this for initialization
    void Start()
    {
        limitsUpDownERC = new float[] { limitsERC[0], limitsArm1[1] };
        limitsShoreShipERC = new float[] { limitsArm2[0], limitsERC[1] };
    }

    void OnEnable()
    {
        changeClampColliders();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
        if (contadorTiempoIniciado)
            contadorTiempo += Time.deltaTime;

        if (isERCActivated)
        {
            actualSpeed = rotateSpeed * 1.5f;
            moveArmPart(shoreShipPart, 2, true, limitsShoreShipERC);
            moveArmPart(upDownPart, 1, false, limitsUpDownERC);
        }

        if ((autoClamp) && (!isClamped))
        {
            //UP DOWN
           
            if (Mathf.Abs(puntaAcople.transform.position.y - objective.transform.position.y) < 1)
                actualSpeed = rotateSpeed / 2;
            else if (Mathf.Abs(puntaAcople.transform.position.y - objective.transform.position.y) >= 1)
                actualSpeed = rotateSpeed * 2;
            if (Mathf.Abs(puntaAcople.transform.position.y - objective.transform.position.y) < 0.01)
            {
                actualSpeed = 0;
                automaticYAccomplished = true;
            }
            if (puntaAcople.transform.position.y > objective.transform.position.y)
            {
                moveArmPart(upDownPart, 1, true, limitsArm1);
            }
            else if (puntaAcople.transform.position.y < objective.transform.position.y)
            {
                moveArmPart(upDownPart, 1, false, limitsArm1);
            }

            //SHIP SHORE
            if (Mathf.Abs(puntaAcople.transform.position.x - objective.transform.position.x) < 1)
                actualSpeed = rotateSpeed / 2;
            else if (Mathf.Abs(puntaAcople.transform.position.x - objective.transform.position.x) >= 1)
                actualSpeed = rotateSpeed * 2;
            if (Mathf.Abs(puntaAcople.transform.position.x - objective.transform.position.x) < 0.01)
            {
                actualSpeed = 0;
                automaticXAccomplished = true;
            }
            if (puntaAcople.transform.position.x < objective.transform.position.x)
            {
                moveArmPart(shoreShipPart, 2, true, limitsArm2);
            }
            else if (puntaAcople.transform.position.x > objective.transform.position.x)
            {
                moveArmPart(shoreShipPart, 2, false, limitsArm2);
            }

            //LEFT RIGHT
            if (Mathf.Abs(puntaAcople.transform.position.z - objective.transform.position.z) < 1)
                actualSpeed = rotateSpeed / 2;
            else if (Mathf.Abs(puntaAcople.transform.position.z - objective.transform.position.z) >= 1)
                actualSpeed = rotateSpeed * 2;
            if (Mathf.Abs(puntaAcople.transform.position.x - objective.transform.position.x) < 0.01)
            {
                actualSpeed = 0;
                automaticZAccomplished = true;
            }
            if (puntaAcople.transform.position.z < objective.transform.position.z)
            {
                isRotating = true;
                rotateArmPart(rotorPart, false, limitsSwivel);
            }
            else if (puntaAcople.transform.position.y > objective.transform.position.y)
            {
                isRotating = true;
                rotateArmPart(rotorPart, true, limitsSwivel);
            }

            if (automaticXAccomplished && automaticYAccomplished && automaticZAccomplished)
            {
                triggerCouplerAnimation();
                autoClamp = false;
            }
        }

        if (isActiveArm)
        {
            if (!isClamped)
            {
                if (!autoClamp)
                {
                    //Movimiento por radio
                    //mover primera tuberia
                    if ((Input.GetAxis("Vertical") < 0) && (Input.GetAxis("Vertical") > -0.5))
                    {
                        
                        //Debug.Log("Moviendo brazo 1 hacia arriba");
                        actualSpeed = rotateSpeed / 2;
                        moveArmPart(upDownPart, 1, true, limitsArm1);
                    }
                    if ((Input.GetAxis("Vertical") > 0) && (Input.GetAxis("Vertical") < 0.5))
                    {
                        //Debug.Log("Moviendo brazo 1 hacia abajo");
                        actualSpeed = rotateSpeed / 2;
                        moveArmPart(upDownPart, 1, false, limitsArm1);
                    }
                    if (Input.GetAxis("Vertical") <= -0.5)
                    {
                        //Debug.Log("Moviendo brazo 1 hacia arriba");
                        actualSpeed = rotateSpeed * 2;
                        moveArmPart(upDownPart, 1, true, limitsArm1);
                    }
                    if (Input.GetAxis("Vertical") >= 0.5)
                    {
                        //Debug.Log("Moviendo brazo 1 hacia abajo");
                        actualSpeed = rotateSpeed * 2;
                        moveArmPart(upDownPart, 1, false, limitsArm1);
                    }

                    //mover segunda tuberia
                    if (Input.GetAxis("RightV") >= 0.9)
                    {
                        //Debug.Log("Moviendo brazo 2 hacia arriba");
                        actualSpeed = rotateSpeed * 2;
                        moveArmPart(shoreShipPart, 2, true, limitsArm2);
                    }
                    if (Input.GetAxis("RightV") <= -0.9)
                    {
                        //Debug.Log("Moviendo brazo 2 hacia abajo");
                        actualSpeed = rotateSpeed * 2;
                        moveArmPart(shoreShipPart, 2, false, limitsArm2);
                    }
                    if ((Input.GetAxis("RightV") > 0) && (Input.GetAxis("RightV") < 0.9))
                    {
                        //Debug.Log("Moviendo brazo 2 hacia arriba");
                        actualSpeed = rotateSpeed / 2;
                        moveArmPart(shoreShipPart, 2, true, limitsArm2);
                    }
                    if ((Input.GetAxis("RightV") < 0) && (Input.GetAxis("RightV") > -0.9))
                    {
                        //Debug.Log("Moviendo brazo 2 hacia abajo");
                        actualSpeed = rotateSpeed / 2;
                        moveArmPart(shoreShipPart, 2, false, limitsArm2);
                    }

                    //Rotar brazo
                    /*if (Input.GetKey (KeyCode.Z)) {
                        Debug.Log("Rotando brazo hacia izquierda");
                        rotateArmPart(rotorPart, true);
                    }
                    if (Input.GetKey (KeyCode.X)) {
                        Debug.Log("Rotando brazo hacia derecha");
                        rotateArmPart(rotorPart, false);
                    }*/
                    if (Input.GetAxis("Horizontal") > 0.5)
                    {
                        //Debug.Log("Rotando brazo hacia izquierda");
                        isRotating = true;
                        actualSpeed = rotateSpeed * 2;
                        rotateArmPart(rotorPart, false, limitsSwivel);
                    }
                    if (Input.GetAxis("Horizontal") < -0.5)
                    {
                        //Debug.Log("Rotando brazo hacia derecha");
                        isRotating = true;
                        actualSpeed = rotateSpeed * 2;
                        rotateArmPart(rotorPart, true, limitsSwivel);
                    }
                    if ((Input.GetAxis("Horizontal") > 0) && (Input.GetAxis("Horizontal") <= 0.5))
                    {
                        //Debug.Log("Rotando brazo hacia izquierda");
                        isRotating = true;
                        actualSpeed = rotateSpeed / 2;
                        rotateArmPart(rotorPart, false, limitsSwivel);
                    }
                    if ((Input.GetAxis("Horizontal") < 0) && (Input.GetAxis("Horizontal") >= -0.5))
                    {
                        //Debug.Log("Rotando brazo hacia derecha");
                        isRotating = true;
                        actualSpeed = rotateSpeed / 2;
                        rotateArmPart(rotorPart, true, limitsSwivel);
                    }

                    //Movimiento local
                    //mover primera tuberia
                    if ((isMovingDown) && (!isFasterThanChiktikkaFastpaws))
                    {
                        //Debug.Log("Moviendo brazo 1 hacia arriba");
                        actualSpeed = rotateSpeed / 2;
                        moveArmPart(upDownPart, 1, true, limitsArm1);
                    }
                    if ((isMovingUp) && (!isFasterThanChiktikkaFastpaws))
                    {
                        //Debug.Log("Moviendo brazo 1 hacia abajo");
                        actualSpeed = rotateSpeed / 2;
                        moveArmPart(upDownPart, 1, false, limitsArm1);
                    }
                    if ((isMovingDown) && (isFasterThanChiktikkaFastpaws))
                    {
                        //Debug.Log("Moviendo brazo 1 hacia arriba");
                        actualSpeed = rotateSpeed * 2;
                        moveArmPart(upDownPart, 1, true, limitsArm1);
                    }
                    if ((isMovingUp) && (isFasterThanChiktikkaFastpaws))
                    {
                        //Debug.Log("Moviendo brazo 1 hacia abajo");
                        actualSpeed = rotateSpeed * 2;
                        moveArmPart(upDownPart, 1, false, limitsArm1);
                    }

                    //mover segunda tuberia
                    if ((isMovingShip) && (isFasterThanChiktikkaFastpaws))
                    {
                        //Debug.Log("Moviendo brazo 2 hacia arriba");
                        actualSpeed = rotateSpeed * 2;
                        moveArmPart(shoreShipPart, 2, true, limitsArm2);
                    }
                    if ((isMovingShore) && (isFasterThanChiktikkaFastpaws))
                    {
                        //Debug.Log("Moviendo brazo 2 hacia abajo");
                        actualSpeed = rotateSpeed * 2;
                        moveArmPart(shoreShipPart, 2, false, limitsArm2);
                    }
                    if ((isMovingShip) && (!isFasterThanChiktikkaFastpaws))
                    {
                        //Debug.Log("Moviendo brazo 2 hacia arriba");
                        actualSpeed = rotateSpeed / 2;
                        moveArmPart(shoreShipPart, 2, true, limitsArm2);
                    }
                    if ((isMovingShore) && (!isFasterThanChiktikkaFastpaws))
                    {
                        //Debug.Log("Moviendo brazo 2 hacia abajo");
                        actualSpeed = rotateSpeed / 2;
                        moveArmPart(shoreShipPart, 2, false, limitsArm2);
                    }

                    //Rotar brazo
                    /*if (Input.GetKey (KeyCode.Z)) {
                        Debug.Log("Rotando brazo hacia izquierda");
                        rotateArmPart(rotorPart, true);
                    }
                    if (Input.GetKey (KeyCode.X)) {
                        Debug.Log("Rotando brazo hacia derecha");
                        rotateArmPart(rotorPart, false);
                    }*/
                    if ((isMovingRight) && (isFasterThanChiktikkaFastpaws))
                    {
                        //Debug.Log("Rotando brazo hacia izquierda");
                        isRotating = true;
                        actualSpeed = rotateSpeed * 2;
                        rotateArmPart(rotorPart, false, limitsSwivel);
                    }
                    if ((isMovingLeft) && (isFasterThanChiktikkaFastpaws))
                    {
                        //Debug.Log("Rotando brazo hacia derecha");
                        isRotating = true;
                        actualSpeed = rotateSpeed * 2;
                        rotateArmPart(rotorPart, true, limitsSwivel);
                    }
                    if ((isMovingRight) && (!isFasterThanChiktikkaFastpaws))
                    {
                        //Debug.Log("Rotando brazo hacia izquierda");
                        isRotating = true;
                        actualSpeed = rotateSpeed / 2;
                        rotateArmPart(rotorPart, false, limitsSwivel);
                    }
                    if ((isMovingLeft) && (!isFasterThanChiktikkaFastpaws))
                    {
                        //Debug.Log("Rotando brazo hacia derecha");
                        isRotating = true;
                        actualSpeed = rotateSpeed / 2;
                        rotateArmPart(rotorPart, true, limitsSwivel);
                    }

                    if ((Input.GetAxis("RightH") == 0) && (!isMovingRight) && (!isMovingLeft))
                    {
                        isRotating = false;
                    }

                }
            }

            //Mostrar limite de alarma
            if (Input.GetKeyDown(KeyCode.A))
            {
                if (!isActiveLimitAlarm)
                {
                    limitAlarmGreen.GetComponent<Renderer>().material = alarmGreen;
                    limitAlarmRed.GetComponent<Renderer>().material = alarmRed;
                    limitAlarmYellow.GetComponent<Renderer>().material = alarmYellow;

                  /*  foreach (GameObject alarmRedObject in limitAlarmsRed)
                    {
                        alarmRedObject.GetComponent<Renderer>().material = alarmRed;
                    }
                   * */
                }
                else
                {
                    limitAlarmGreen.GetComponent<Renderer>().material = alarmTransparent;
                    limitAlarmRed.GetComponent<Renderer>().material = alarmTransparent;
                    limitAlarmYellow.GetComponent<Renderer>().material = alarmTransparent;

                   /* foreach (GameObject alarmRedObject in limitAlarmsRed)
                    {
                        alarmRedObject.GetComponent<Renderer>().material = alarmTransparent;
                    } */
                }

                isActiveLimitAlarm = !isActiveLimitAlarm;
            }

            if (((Input.GetKeyDown(KeyCode.E)) || Input.GetButtonDown("CouplerInput") || (doCoupling)) && (!blockCouplerAnimation))
            {
                blockCouplerAnimation = true;
                Debug.Log("Coupler triggered");
                triggerCouplerAnimation();
            }

            if (Input.GetKeyDown(KeyCode.Q) /*|| Input.GetButtonDown("ManualERC")*/)
            {
                triggerERCAnimation();
            }

            if (Input.GetKeyDown(KeyCode.P))
            {
                Debug.Log(proportioner);
            }

            if (Input.GetKeyDown(KeyCode.P))
            {
                objective = getClosestManifold();
                if (objective != null)
                    autoClamp = true;
            }
        }

        if (isClamped)
        {
            if (Input.GetButtonDown("FillShip"))
            {
                Debug.Log("Filling ship");
                isFillingVessel = true;
            }
        }

        if (isFillingVessel)
        {
            fillShip();
        }

        if (isFreeWheel)
        {
            freeWheel();
        }

        if ((isClamped) && (enableERC) && (!isJustOnceERC))
        {
            triggerERCAnimation();
            isJustOnceERC = true;
        }
        else if ((!isClamped) && (enableERC))
        {
            enableERC = false;
        }
    }

    public GameObject getClosestManifold()
    {
        GameObject closestManifold = null;
        float distance = 1000000f;
        foreach (GameObject scoreCollider in clampingScoreColliders)
        {
            ClampingScore clampingScore = scoreCollider.GetComponent<ClampingScore>();
            float newDistance = Vector3.Distance(scoreCollider.transform.position, puntaAcople.transform.position);
            if ((newDistance < distance) && (!clampingScore.hasEntered))
            {
                distance = newDistance;
                closestManifold = scoreCollider;
            }
        }

        return closestManifold;
    }

    public void initializeMovements()
    {
        isMovingShip = false;
        isMovingShore = false;
        isMovingUp = false;
        isMovingDown = false;
        isMovingLeft = false;
        isMovingRight = false;
    }

    void fillShip()
    {
        Rigidbody vesselRigidBody = vesselFloatingContainer.GetComponent<Rigidbody>();
        if (vesselRigidBody.mass <= 2)
            vesselRigidBody.mass += 0.01f * Time.deltaTime * 10000000;
    }

    void freeWheel()
    {
        ConfigurableJoint hinge1 = (ConfigurableJoint)upDownPart.GetComponent("ConfigurableJoint");
        ConfigurableJoint hinge2 = (ConfigurableJoint)shoreShipPart.GetComponent("ConfigurableJoint");

        /*JointDrive manualDrive = hinge1.angularXDrive;
        manualDrive.positionDamper = 200000;
        manualDrive.positionSpring = 200000;
        manualDrive.maximumForce = 5000;
		
        hinge1.angularXDrive = manualDrive;
        hinge2.angularXDrive = manualDrive;*/

        Quaternion targetRotation1 = Quaternion.AngleAxis((360 - upDownPart.transform.localRotation.eulerAngles.x), Vector3.right);
        hinge1.targetRotation = targetRotation1;

        Quaternion targetRotation2 = Quaternion.AngleAxis((-shoreShipPart.transform.localRotation.eulerAngles.x), Vector3.right);
        hinge2.targetRotation = targetRotation2;

        upDownPartRotation = (360 - upDownPart.transform.localRotation.eulerAngles.x);
        shoreShipPartRotation = (-shoreShipPart.transform.localRotation.eulerAngles.x);

        JointDrive freeWheelDrive = hinge1.angularXDrive;
        freeWheelDrive.positionDamper = 0;
        freeWheelDrive.positionSpring = 0;
        freeWheelDrive.maximumForce = 0;

        hinge1.angularXDrive = freeWheelDrive;
        hinge2.angularXDrive = freeWheelDrive;

        //Debug.Log ("Freewheeeeeeeeeeeeeeeeeeeeeeeeely");
    }

    void triggerERCAnimation()
    {

        ERCTop.GetComponent<Animation>()["Take 001_copy"].time = 0;
        ERCTop.GetComponent<Animation>()["Take 001_copy"].speed = 1;
        ERCTop.GetComponent<Animation>().Play("Take 001_copy");

        ERCBottom.GetComponent<Animation>()["Take 001_copy"].time = 0;
        ERCBottom.GetComponent<Animation>()["Take 001_copy"].speed = 1;
        ERCBottom.GetComponent<Animation>().Play("Take 001_copy");
        StartCoroutine(ERCVesselInteraction());
    }

    IEnumerator ERCVesselInteraction()
    {
        yield return new WaitForSeconds(3.5f);

        isFreeWheel = false;

        ConfigurableJoint hinge1 = (ConfigurableJoint)upDownPart.GetComponent("ConfigurableJoint");
        ConfigurableJoint hinge2 = (ConfigurableJoint)shoreShipPart.GetComponent("ConfigurableJoint");

        JointDrive manualDrive = hinge1.angularXDrive;
        manualDrive.positionDamper = 200000;
        manualDrive.positionSpring = 200000;
        manualDrive.maximumForce = 5000;

        hinge1.angularXDrive = manualDrive;
        hinge2.angularXDrive = manualDrive;

        ERCTopContainer.transform.parent = shoreShipPart.transform;

        if (isClamped)
        {
            ERCBottomContainer.transform.parent = vessel.transform;
            isClamped = false;
            isERCActivated = true;
            Rigidbody partRigidBody = (Rigidbody)rotorPart.GetComponent("Rigidbody");
            partRigidBody.constraints = RigidbodyConstraints.FreezeAll;
            jointForFreeWheel.SetActive(false);
        }
        else
        {
            Rigidbody gameObjectsRigidBody = ERCBottomContainer.AddComponent<Rigidbody>();
            //CapsuleCollider gameObjectsCollider = ERCBottomContainer.GetComponent<CapsuleCollider>();
            BoxCollider gameObjectsCollider2 = ERCBottomContainer.GetComponent<BoxCollider>();
            hoseCollidersContainer.SetActive(false);
            //gameObjectsRigidBody.mass = 0.1f;
            //gameObjectsCollider.enabled = true;
            gameObjectsCollider2.enabled = true;
            Buoyancy boyancia = ERCBottomContainer.GetComponent<Buoyancy>();
            boyancia.enabled = true;
        }


    }

    public void triggerCouplerAnimation()
    {

        if (!isClamped)
        {
            Debug.Log("STARTING EVALUATION");
            bool isInPosition = false;
            foreach (GameObject scoreCollider in clampingScoreColliders)
            {
                Debug.Log("Coupling");
                ClampingScore clampingScore = scoreCollider.GetComponent<ClampingScore>();
                if ((clampingScore.hasEntered) && (puntaAcople == clampingScore.armPoint))
                {
                    Debug.Log("EVALUATION CONFIRMED CLAMPING");
                    isInPosition = true;
                    clampingScore.putScore();

                    Debug.Log("Distancia de separación: " + (Mathf.Round(clampingScore.score * 100f) / 100f) + " Mts."
                        + ". X: " + (Mathf.Round(clampingScore.distanceX * 100f) / 100f) + " Mts."
                         + ". Y: " + (Mathf.Round(clampingScore.distanceY * 100f) / 100f) + " Mts."
                          + ". Z: " + (Mathf.Round(clampingScore.distanceZ * 100f) / 100f) + " Mts."
                          + ". Tiempo: " + (Mathf.Round(contadorTiempo * 100f) / 100f) + " Seg.");

                    scoreText.text = "Distancia: " + (Mathf.Round(clampingScore.score * 100f) / 100f) + " Mts."
                        + "\n X: " + (Mathf.Round(clampingScore.distanceX * 100f) / 100f) + " Mts."
                         + "\n Y: " + (Mathf.Round(clampingScore.distanceY * 100f) / 100f) + " Mts."
                          + "\n Z: " + (Mathf.Round(clampingScore.distanceZ * 100f) / 100f) + " Mts."
                          + "\n Tiempo: " + (Mathf.Round(contadorTiempo * 100f) / 100f) + " Seg.";
                }
            }

            Debug.Log("Animando hacia adelante.");

            couplerClamps.GetComponent<Animation>()["Take 001_copy"].time = 0;
            couplerClamps.GetComponent<Animation>()["Take 001_copy"].speed = 1;
            couplerClamps.GetComponent<Animation>().Play("Take 001_copy");

            if (isInPosition)
            {
                StartCoroutine(anchorToVessel());
            }
            else
            {
                Debug.Log("EVALUATION DENIED COUPLING");
            }
        }
        else
        {
            Debug.Log("Animando hacia atrás.");

            couplerClamps.GetComponent<Animation>()["Take 001_copy"].time = couplerClamps.GetComponent<Animation>()["Take 001_copy"].length;
            couplerClamps.GetComponent<Animation>()["Take 001_copy"].speed = -1;
            couplerClamps.GetComponent<Animation>().Play("Take 001_copy");

            StartCoroutine(detachFromVessel());
        }

        StartCoroutine(enableCouplerAnimarionDelayed());

        isClamped = !isClamped;

        doCoupling = false;

        changeClampColliders();
    }

    IEnumerator anchorToVessel()
    {
        disableClampSupportColliders();

        yield return new WaitForSeconds(couplerDelay);

        jointForFreeWheel.SetActive(true);

        Rigidbody partRigidBody = (Rigidbody)rotorPart.GetComponent("Rigidbody");

        partRigidBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;

        disableAllColliders();

        isFreeWheel = true;
    }

    IEnumerator detachFromVessel()
    {
        yield return new WaitForSeconds(couplerDelay);

        jointForFreeWheel.SetActive(false);

        enableAllColliders();
        changeClampColliders();

        enableClampSupportColliders();

        isFreeWheel = false;
    }

    IEnumerator enableCouplerAnimarionDelayed()
    {
        yield return new WaitForSeconds(couplerDelay);
        blockCouplerAnimation = false;
    }

    void rotateArmPart(GameObject part, bool direction, float[] limits)
    {
        contadorTiempoIniciado = true;

        if (isRotating)
        {
            int directionNumber = 1;
            if (direction)
            {
                directionNumber = 1;
            }
            else
            {
                directionNumber = -1;
            }

            Rigidbody partRigidBody = (Rigidbody)part.GetComponent("Rigidbody");

            bool isMovable = false;
            //if(((part.transform.rotation.y * 100) <= limits[1]) && ((part.transform.rotation.y * 100) >= limits[0])){
            if ((directionNumber == -1) && ((part.transform.rotation.y * 100) <= limits[1]))
                isMovable = true;

            if ((directionNumber == 1) && ((part.transform.rotation.y * 100) >= limits[0]))
                isMovable = true;

            if (isMovable)
            {
                partRigidBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;
                part.transform.Rotate(-Vector3.up * (0.5f * actualSpeed) * Time.deltaTime * directionNumber);
                partRigidBody.constraints = RigidbodyConstraints.FreezeAll;
            }
        }
    }

    void moveArmPart(GameObject part, int selector, bool direction, float[] limits)
    {

        contadorTiempoIniciado = true;

        int directionNumber = 1;
        if (direction)
        {
            directionNumber = 1;
        }
        else
        {
            directionNumber = -1;
        }

        if (selector == 1)
        {
            targetAngle = upDownPartRotation;
        }
        else if (selector == 2)
        {
            targetAngle = shoreShipPartRotation;
        }

        /*targetAngle += Time.deltaTime * rotateSpeed * directionNumber;
        ConfigurableJoint confJoint = (ConfigurableJoint)part.GetComponent ("ConfigurableJoint");
        Quaternion targetRotation = getJointRotation (confJoint);
        SetTargetRotationLocal (confJoint, Quaternion.AngleAxis( targetAngle, Vector3.up ), targetRotation);
        confJoint.targetRotation = targetRotation;*/


        targetAngle += directionNumber * Time.deltaTime * actualSpeed;

        if ((targetAngle >= limits[0]) && (targetAngle <= limits[1]))
        {
            Quaternion targetRotation = Quaternion.AngleAxis(targetAngle, Vector3.right);
            ConfigurableJoint confJoint = (ConfigurableJoint)part.GetComponent("ConfigurableJoint");
            confJoint.targetRotation = targetRotation;

            if (selector == 1)
            {
                upDownPartRotation = targetAngle;
            }
            else if (selector == 2)
            {
                shoreShipPartRotation = targetAngle;
            }

            //Debug.Log ("Angulo de Physx: " + targetAngle + ", Rotacion en Unity " + part.transform.localRotation.eulerAngles.x + "\n");
            //proportioner += targetAngle + "\t" + (360 - part.transform.eulerAngles.x) + "\n";

        }
        else if (isERCActivated)
        {
            Debug.Log("Deactivating ERC");
            isERCActivated = !isERCActivated;
        }

        //Debug.Log (targetAngle);

        //Debug.Log (confJoint.targetRotation.x);
    }

    void stopArmPart(GameObject part)
    {
        /*HingeJoint hinge = (HingeJoint)part.GetComponent ("HingeJoint");
		
        JointMotor motor = hinge.motor;
		
        motor.force = 0;
        motor.targetVelocity = 0;
        motor.freeSpin = false;
		
        hinge.motor = motor;*/

    }

    public Quaternion getJointRotation(ConfigurableJoint joint)
    {
        return (Quaternion.FromToRotation(joint.axis, joint.connectedBody.transform.rotation.eulerAngles));
    }

    /// <summary>
    /// Sets a joint's targetRotation to match a given local rotation.
    /// The joint transform's local rotation must be cached on Start and passed into this method.
    /// </summary>
    public static void SetTargetRotationLocal(ConfigurableJoint joint, Quaternion targetLocalRotation, Quaternion startLocalRotation)
    {
        if (joint.configuredInWorldSpace)
        {
            Debug.LogError("SetTargetRotationLocal should not be used with joints that are configured in world space. For world space joints, use SetTargetRotation.", joint);
        }
        SetTargetRotationInternal(joint, targetLocalRotation, startLocalRotation, Space.Self);
    }
    /// <summary>
    /// Sets a joint's targetRotation to match a given world rotation.
    /// The joint transform's world rotation must be cached on Start and passed into this method.
    /// </summary>
    public static void SetTargetRotation(ConfigurableJoint joint, Quaternion targetWorldRotation, Quaternion startWorldRotation)
    {
        if (!joint.configuredInWorldSpace)
        {
            Debug.LogError("SetTargetRotation must be used with joints that are configured in world space. For local space joints, use SetTargetRotationLocal.", joint);
        }
        SetTargetRotationInternal(joint, targetWorldRotation, startWorldRotation, Space.World);
    }
    static void SetTargetRotationInternal(ConfigurableJoint joint, Quaternion targetRotation, Quaternion startRotation, Space space)
    {
        // Calculate the rotation expressed by the joint's axis and secondary axis
        var right = joint.axis;
        var forward = Vector3.Cross(joint.axis, joint.secondaryAxis).normalized;
        var up = Vector3.Cross(forward, right).normalized;
        Quaternion worldToJointSpace = Quaternion.LookRotation(forward, up);
        // Transform into world space
        Quaternion resultRotation = Quaternion.Inverse(worldToJointSpace);
        // Counter-rotate and apply the new local rotation.
        // Joint space is the inverse of world space, so we need to invert our value
        if (space == Space.World)
        {
            resultRotation *= startRotation * Quaternion.Inverse(targetRotation);
        }
        else
        {
            resultRotation *= Quaternion.Inverse(targetRotation) * startRotation;
        }
        // Transform back into joint space
        resultRotation *= worldToJointSpace;
        // Set target rotation to our newly calculated rotation
        joint.targetRotation = resultRotation;
    }

    private void changeClampColliders()
    {
        StartCoroutine(delayChangeClampColliders());
    }

    IEnumerator delayChangeClampColliders()
    {
        yield return new WaitForSeconds(1.5f);
        for (int i = 0; i < clampColliders.Length; i++)
        {
            clampColliders[i].SetActive(!clampColliders[i].activeSelf);
        }
    }

    private void disableAllColliders()
    {
        for (int i = 0; i < allColliders.Length; i++)
        {
            allColliders[i].SetActive(false);
        }
    }

    private void enableAllColliders()
    {
        for (int i = 0; i < allColliders.Length; i++)
        {
            allColliders[i].SetActive(true);
        }
    }

    private void disableClampSupportColliders()
    {
        for (int i = 0; i < clampSupportColliders.Length; i++)
        {
            clampSupportColliders[i].SetActive(false);
        }
    }

    private void enableClampSupportColliders()
    {
        for (int i = 0; i < clampSupportColliders.Length; i++)
        {
            clampSupportColliders[i].SetActive(true);
        }
    }
}
