﻿using UnityEngine;
using System.Collections;

public class DestoyAfterAWhile : MonoBehaviour {

    public float deactivationTime = 2f;

	// Use this for initialization
	void Start () {
        StartCoroutine(proceedToDestroy());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    IEnumerator proceedToDestroy()
    {
        yield return new WaitForSeconds(deactivationTime);
        Destroy(gameObject);
    }
}
