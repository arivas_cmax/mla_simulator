﻿using UnityEngine;
using System.Collections;

public class mostrarVideo : MonoBehaviour
{
    public GameObject Video;
    private int primera = 0;

    MovieTexture ActualMovie;

    public void PlayVideo(MovieTexture movTexture)
    {
        Video.GetComponent<Renderer>().material.mainTexture = movTexture;
        movTexture.Play();
        ActualMovie = movTexture;
 
    }
    public void StopVideo()
    {
        ActualMovie.Stop();
    }
}
