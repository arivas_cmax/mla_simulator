﻿using UnityEngine;
using System.Collections;

public class SyncCoupling : Photon.MonoBehaviour {

    public bool triggerCoupler = false;
    public int activeArm = 0;

    public GameObject[] armControllers;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (!PhotonNetwork.offlineMode)
        {
            if ((Input.GetKeyDown(KeyCode.Alpha1)) || Input.GetButtonDown("Fire1"))
            {
                activeArm = 0;
            }

            if ((Input.GetKeyDown(KeyCode.Alpha2)) || Input.GetButtonDown("Jump"))
            {
                activeArm = 1;
            }

            if ((Input.GetKeyDown(KeyCode.Alpha3)) || Input.GetButtonDown("Fire3"))
            {
                activeArm = 2;
            }

            if ((Input.GetKeyDown(KeyCode.E)) || Input.GetButtonDown("Fire2"))
            {
                triggerCoupler = true;
            }

            if (triggerCoupler)
            {
                if (armControllers[activeArm].GetComponent<LoadingArmController>().remotelyControlled)
                {
                    Debug.Log("TRIGGER COUPLER");
                    armControllers[activeArm].GetComponent<LoadingArmController>().triggerCouplerAnimation();
                    triggerCoupler = false;
                }
            }
        }
        
	}

    private IEnumerator RevertTrigger()
    {
        yield return new WaitForSeconds(3.5f);
        triggerCoupler = false;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext((bool)triggerCoupler);
            stream.SendNext((int)activeArm);            
        }
        else
        {
            this.triggerCoupler = (bool)stream.ReceiveNext();
            this.activeArm = (int)stream.ReceiveNext();
        }
    }
}
