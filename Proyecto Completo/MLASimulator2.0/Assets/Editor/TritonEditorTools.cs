﻿#if (!UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_4 && !UNITY_4_5 && !UNITY_4_6)
#define UNITY_5_OR_NEWER
#endif

using UnityEditor;
using UnityEngine;
using System.Diagnostics;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class TritonEditorTools : MonoBehaviour
{
#if UNITY_5_OR_NEWER
	//Update Plugin Importer Settings to reflect which plugins are 32-bit or 64-bit.
	[MenuItem("Triton Editor Tools/Update Plugin Import Settings")]
	public static void UpdatePluginImportSettings() {
		
		try {
			Update32BitPlugins (new DirectoryInfo("Assets/Plugins/x86"));
			Update32BitPlugins (new DirectoryInfo("Assets/Triton/TritonResources/dll"));
			Update32BitPlugins (new DirectoryInfo("Assets/Triton/TritonResources/vc10/win32"));
			
			Update64BitPlugins (new DirectoryInfo("Assets/Plugins/x86_64"));
			Update64BitPlugins (new DirectoryInfo ("Assets/Triton/TritonResources/dll64"));
			Update64BitPlugins (new DirectoryInfo ("Assets/Triton/TritonResources/vc10/x64"));
		} catch(System.Exception e) {
			UnityEngine.Debug.Log (e.Message);
			UnityEngine.Debug.Log ("Update Plugin Import Settings Failed");
		}
		
		UnityEngine.Debug.Log ("Plugin import settings updated.");
	}
	
	private static void Update32BitPlugins(DirectoryInfo directory) {
		
		FileInfo[] files = directory.GetFiles ("*.dll");
		
		foreach(FileInfo file in files) {
			UpdatePlugin (file, "x86");
		}
	}
	private static void Update64BitPlugins(DirectoryInfo directory) {
		FileInfo[] files = directory.GetFiles ("*.dll");
		
		foreach(FileInfo file in files) {
			UpdatePlugin (file, "x86_64");
		}
	}
	
	private static void UpdatePlugin(FileInfo file, string architectureString) {
		string relativeFilePath = file.FullName;
		relativeFilePath = relativeFilePath.Substring (relativeFilePath.IndexOf ("Assets"));
		
		PluginImporter pluginImporter = AssetImporter.GetAtPath (relativeFilePath) as PluginImporter;

#if UNITY_STANDALONE_WIN
		pluginImporter.SetPlatformData (BuildTarget.StandaloneWindows, "CPU", architectureString);
		pluginImporter.SetPlatformData (BuildTarget.StandaloneWindows64, "CPU", architectureString);
		pluginImporter.SetEditorData ("CPU", architectureString);
#endif

#if UNITY_STANDALONE_OSX
		pluginImporter.SetPlatformData (BuildTarget.StandaloneOSXIntel, "CPU", architectureString);
		pluginImporter.SetPlatformData (BuildTarget.StandaloneOSXIntel64, "CPU", architectureString);
		pluginImporter.SetPlatformData (BuildTarget.StandaloneOSXUniversal, "CPU", architectureString);
		pluginImporter.SetEditorData ("CPU", architectureString);
#endif
		
		pluginImporter.SaveAndReimport();
	}
#endif

#if UNITY_STANDALONE_WIN

	//Build 32-bit version of Triton.
	[MenuItem("Triton Editor Tools/Build 32-bit Triton")]
	public static void BuildGame32() {

		DirectoryInfo assetDirectory = new DirectoryInfo ("Assets");

		DirectoryInfo tritonResourcesDirectory = new DirectoryInfo ("Assets/Triton/TritonResources");
		DirectoryInfo pluginsDirectory = new DirectoryInfo ("Assets/Plugins");

        try
        {
            Update32BitPlugins(new DirectoryInfo("Assets/Plugins/x86"));
            Update32BitPlugins(new DirectoryInfo("Assets/Triton/TritonResources/dll"));
            Update32BitPlugins(new DirectoryInfo("Assets/Triton/TritonResources/vc10/win32"));
        }
        catch (System.Exception e)
        {
            UnityEngine.Debug.Log(e.Message);
            UnityEngine.Debug.Log("Update Plugin Import Settings Failed");
        }

		BuildGame (assetDirectory, tritonResourcesDirectory, pluginsDirectory, true);
	}
	
	//Build 64-bit version of Triton.
	[MenuItem("Triton Editor Tools/Build 64-bit Triton")]
	public static void BuildGame64 () {
		
		DirectoryInfo assetDirectory = new DirectoryInfo ("Assets");
		DirectoryInfo tritonResourcesDirectory = new DirectoryInfo ("Assets/Triton/TritonResources");
		DirectoryInfo pluginsDirectory = new DirectoryInfo ("Assets/Plugins/x86_64");

        try
        {
            Update64BitPlugins(new DirectoryInfo("Assets/Plugins/x86_64"));
            Update64BitPlugins(new DirectoryInfo("Assets/Triton/TritonResources/dll64"));
            Update64BitPlugins(new DirectoryInfo("Assets/Triton/TritonResources/vc10/x64"));
        }
        catch (System.Exception e)
        {
            UnityEngine.Debug.Log(e.Message);
            UnityEngine.Debug.Log("Update Plugin Import Settings Failed");
        }

		BuildGame (assetDirectory, tritonResourcesDirectory, pluginsDirectory, false);
	}


    private static void BuildGame(DirectoryInfo assetDirectory,
                                  DirectoryInfo tritonResourcesDirectory,
                                  DirectoryInfo pluginsDirectory,
                                  bool is32Bit)
    {
        //Set D3D11ForceExclusiveMode for Unity 4.6 Triton.
#if UNITY_4_6
		PlayerSettings.d3d11ForceExclusiveMode = false;
#endif
        // Get path of where executable is saved from user.
        string path = EditorUtility.SaveFolderPanel("Build Triton", "", "");

        if (assetDirectory.Exists
           && tritonResourcesDirectory.Exists
           && pluginsDirectory.Exists
           && path.Length > 0)
        {
            FileInfo[] files = assetDirectory.GetFiles("*.unity");

            if (files.Length != 0)
            {

                //Get all names of scene files.
                List<string> scenes = new List<string>();

                foreach (FileInfo file in files)
                {

                    scenes.Add(file.FullName);

                }
                try
                {
                    AssetDatabase.Refresh();

                    if (is32Bit)
                    {
                        //Build 32 bit version of Triton.
                        UnityEngine.Debug.Log("Triton 32-bit building");
                        Build32BitTriton(scenes.ToArray(), path, tritonResourcesDirectory, pluginsDirectory);
                    }
                    else
                    {
                        //Build 64 bit version of Triton.
                        UnityEngine.Debug.Log("Triton 64-bit building");
                        Build64BitTriton(scenes.ToArray(), path, tritonResourcesDirectory, pluginsDirectory);
                    }

                    UnityEngine.Debug.Log("Build complete.");
                }
                catch (System.Exception e)
                {

                    UnityEngine.Debug.LogError(e);
                    UnityEngine.Debug.LogError("Build failed.");
                }
            }
        }
    }
	
	//Windows - copy over Triton Resources and .dlls next to .exe executable (32 bit). Copy TritonDLL in Data/Plugins/x86 (Unity 5)
	private static void Build32BitTriton(string[] scenes, string path, DirectoryInfo resources, DirectoryInfo plugins) {

		string executablePath = string.Empty;

		
		executablePath = "/x86/Triton.exe";
		

		BuildPipeline.BuildPlayer (scenes, path + executablePath, BuildTarget.StandaloneWindows, BuildOptions.ShowBuiltPlayer);
		
		CopyTritonResourcesToExe(path + "/x86", resources);
		CopyPluginToExe(path + "/x86", plugins);
		#if !UNITY_5_OR_NEWER
			CopyPluginToExe (path + "/x86/Triton_Data/Plugins", plugins);
		
		#else
			CopyPluginToExe (path + "/x86/Triton_Data/Plugins", plugins);
		#endif
	}

	//Windows - copy over Triton Resources and .dlls next to .exe executable (64 bit). Copy TritonDLL in Data/Plugins/x86_64 (Unity5)
	private static void Build64BitTriton(string[] scenes, string path, DirectoryInfo resources, DirectoryInfo plugins) {

		string executablePath = string.Empty;
		

		executablePath = "/x86_64/Triton.exe";
		

		BuildPipeline.BuildPlayer(scenes, path + executablePath, BuildTarget.StandaloneWindows64, BuildOptions.ShowBuiltPlayer);
		
		CopyTritonResourcesToExe(path + "/x86_64", resources);
		CopyPluginToExe(path + "/x86_64", plugins);
		#if !UNITY_5_OR_NEWER
		CopyPluginToExe (path + "/x86_64/Triton_Data/Plugins", plugins);
		#else
		
			CopyPluginToExe (path + "/x86_64/Triton_Data/Plugins", plugins);
		
		#endif
	}

	//Copy Triton Resources next to executable
	private static void CopyTritonResourcesToExe(string path, DirectoryInfo resources) {
		
		DirectoryInfo newDirectory = Directory.CreateDirectory (path + "/" + resources.Name);
		
		FileInfo[] files = resources.GetFiles ();
		
		foreach(FileInfo file in files) {
			if(Path.GetExtension(file.FullName) != ".meta") {
				file.CopyTo(newDirectory.FullName + "/" + file.Name, true);
                if (!File.Exists(newDirectory.FullName + "/" + file.Name)) {
                    UnityEngine.Debug.LogError("Unable to copy file " + newDirectory.FullName + "/" + file.Name + ". Please manually copy the file or the build will not work");
                }
			}
		}
           
		
		DirectoryInfo[] subDirectories = resources.GetDirectories ();
		
		foreach(DirectoryInfo subDirectory in subDirectories) {
			CopyTritonResourcesToExe (newDirectory.FullName, subDirectory);
		}
	}

	private static void CopyPluginToExe(string path, DirectoryInfo plugins) {
		FileInfo[] files = plugins.GetFiles ("*.dll", SearchOption.AllDirectories);
		
		foreach(FileInfo file in files) {
			if(file.Name == "TritonDLL.dll") {
				file.CopyTo(path + "/TritonDLL.dll", true);
				break;
			}
		}
	}
#endif
}
