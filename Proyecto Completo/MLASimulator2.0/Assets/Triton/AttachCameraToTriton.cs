using UnityEngine;
using System.Collections;

public class AttachCameraToTriton : MonoBehaviour {

	// Use this for initialization
	void Start () {
		TritonUnity triton = (TritonUnity)(GameObject.FindObjectOfType(typeof(TritonUnity)));
		if (triton != null)
		{
			triton.AddCamera(gameObject.GetComponent<Camera>());	
		}
	}
}
