﻿using UnityEngine;
using System.Collections;

public class LocalCouplerClose : MonoBehaviour {

	public GameObject controller;
	private bool itsFirstTime = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		if((other.tag == "Player") && (itsFirstTime))
		{
			Debug.Log("Close");
			LoadingArmSelector scriptSelector = controller.GetComponent<LoadingArmSelector>();
			LoadingArmController scriptController = scriptSelector.getCurrentArm().GetComponent<LoadingArmController>();
			if(!scriptController.isClamped)
				scriptController.doCoupling = true;

			itsFirstTime = false;
		}
	}
	
	void OnTriggerExit(Collider other){
		if(other.tag == "Player")
		{
			itsFirstTime = true;
		}
	}
}
