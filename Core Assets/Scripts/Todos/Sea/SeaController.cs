﻿using UnityEngine;
using System.Collections;

public class SeaController : MonoBehaviour {

	public GameObject buoyancyCapableObject;
	public GameObject buoyancyUncapableObject;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.PageUp))
		{
			TritonUnity triton = (TritonUnity)(GameObject.FindObjectOfType(typeof(TritonUnity)));
			//triton.GetOcean().SetRefractionColor(new Triton.Vector3(1, 0, 0));
			triton.windSpeed += 1;
		}
		
		if (Input.GetKeyDown(KeyCode.PageDown))
		{
			TritonUnity triton = (TritonUnity)(GameObject.FindObjectOfType(typeof(TritonUnity)));
			//triton.GetOcean().SetRefractionColor(new Triton.Vector3(1, 0, 0));
			triton.windSpeed -= 1;
		}

		if (Input.GetKeyDown(KeyCode.B))
		{
			Debug.Log ("Colocando embarcacion en un estado flotante.");
			buoyancyUncapableObject.transform.parent = buoyancyCapableObject.transform;
		}
	}
}
