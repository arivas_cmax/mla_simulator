﻿using UnityEngine;
using System.Collections;
using System.Threading;

public class StateManager {

	private static StateManager instance = null;

    public StateMachine stateMachine;
    public StateParameters stateParameters;

	public string mensaje = "Singleton funcionando correctamente.";

	private StateManager(){
        
	}

	public static StateManager getInstance(){
		if(instance == null)
		{
			instance = new StateManager();

            instance.stateMachine = StateMachine.getInstance();
            instance.stateParameters = StateParameters.getInstance();

			return instance; 
		}else{
			return instance; 
		}
	}

    public void startStateMachine()
    {
        Thread thread = new Thread(new ThreadStart(stateMachine.start));
        thread.Start();
    }
}
