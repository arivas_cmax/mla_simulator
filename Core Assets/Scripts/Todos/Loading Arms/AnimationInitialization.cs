﻿using UnityEngine;
using System.Collections;

public class AnimationInitialization : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<Animation>()["Take 001_copy"].time = 0;
		GetComponent<Animation>()["Take 001_copy"].speed = 0;
		GetComponent<Animation>().Play("Take 001_copy");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
