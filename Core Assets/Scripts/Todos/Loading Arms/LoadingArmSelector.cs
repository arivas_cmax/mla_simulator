﻿using UnityEngine;
using System.Collections;

public class LoadingArmSelector : MonoBehaviour {

	public GameObject brazo1;
	public GameObject brazo2;
	public GameObject brazo3;

	private GameObject[] brazos;
	private int brazoActual;
    // PlayerOrMLA: True-> MLA  /  False -> Player
    private bool PlayerOrMLA; // Controla el selector para mover el player/camara o los brazos 

   

	// Use this for initialization
	void Start () {
		LoadingArmController script = brazo1.GetComponent<LoadingArmController>();
		script.isActiveArm = true;

		script = brazo2.GetComponent<LoadingArmController>();
		script.isActiveArm = false;

		script = brazo2.GetComponent<LoadingArmController>();
		script.isActiveArm = false;

		brazos = new GameObject[]{brazo1, brazo2, brazo3};
		brazoActual = 0;
	}
	
	// Update is called once per frame
	void Update () {
      /*  if ((Input.GetKeyDown(KeyCode.Alpha1)) || Input.GetButton("Fire1"))
        {
            Debug.Log("Brazo 1");
            ChangeMLAController(0);           
        }

        if ((Input.GetKeyDown(KeyCode.Alpha2)) || Input.GetButtonDown("Jump"))
        {
            ChangeMLAController(3);  // Se iguala a 3 para que entre en modo defaul
        }

        if ((Input.GetKeyDown(KeyCode.Alpha3)) || Input.GetButtonDown("Fire3"))
        {
            ChangeMLAController(1);             
        }
        */
        if (Input.GetButton("PlayerOrMLA"))
        {
            PlayerOrMLA = true;
        }
        else
        {
            LoadingArmController script = brazo1.GetComponent<LoadingArmController>();
            script.isActiveArm = false;

            script = brazo2.GetComponent<LoadingArmController>();
            script.isActiveArm = false;

            script = brazo3.GetComponent<LoadingArmController>();
            script.isActiveArm = false;
            PlayerOrMLA = false;
        }            

        if (Input.GetButton("Fire3") || Input.GetButton("Fire1")) 
          {
              if ((Input.GetKeyDown(KeyCode.Alpha1)) || Input.GetButton("Fire1"))
              {
                  ChangeMLAController(0);
              }
              if ((Input.GetKeyDown(KeyCode.Alpha3)) || Input.GetButton("Fire3"))
              {
                  ChangeMLAController(1);
              }  
          }
          else
          {
              ChangeMLAController(3);  // Se iguala a 3 para que entre en modo defaul
          }  
         
	}

	public GameObject getCurrentArm(){
		Debug.Log("Retornando el brazo: " + brazoActual + ". " + brazos[brazoActual]);
		return brazos[brazoActual];
	}

    public void ChangeMLAController(int brazoHandler){
        if (PlayerOrMLA)
        {
            switch (brazoHandler)
            {
                case 0:
                    LoadingArmController script = brazo1.GetComponent<LoadingArmController>();
                    script.isActiveArm = true;

                    script = brazo2.GetComponent<LoadingArmController>();
                    script.isActiveArm = false;

                    script = brazo3.GetComponent<LoadingArmController>();
                    script.isActiveArm = false;
                    break;
                     
                case 1:
                    script = brazo1.GetComponent<LoadingArmController>();
                    script.isActiveArm = false;

                    script = brazo2.GetComponent<LoadingArmController>();
                    script.isActiveArm = false;

                    script = brazo3.GetComponent<LoadingArmController>();
                    script.isActiveArm = true;
                    break;

                default:
                    script = brazo1.GetComponent<LoadingArmController>();
                    script.isActiveArm = false;

                    script = brazo2.GetComponent<LoadingArmController>();
                    script.isActiveArm = true;

                    script = brazo3.GetComponent<LoadingArmController>();
                    script.isActiveArm = false;
                    break;
            }
        }
        
    }

	public void nextArm()
	{
		if(brazoActual >= brazos.Length-1)
		{
			brazoActual = 0;
		}else{
			brazoActual++;
		}

		deactivateAllArms();

		GameObject brazo = brazos[brazoActual];
		LoadingArmController script = brazo.GetComponent<LoadingArmController>();
		script.isActiveArm = true;

		Debug.Log("Active arm: " + brazoActual);
	}

	private void deactivateAllArms(){
		foreach(GameObject brazo in brazos)
		{
			LoadingArmController script = brazo.GetComponent<LoadingArmController>();
			script.isActiveArm = false;
		}
	}
}
