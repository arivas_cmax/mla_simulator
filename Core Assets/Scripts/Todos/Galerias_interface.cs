﻿using UnityEngine;
using System.Collections;

public class Galerias_interface : MonoBehaviour 
{
    public GameObject Objeto_01;
    public GameObject Objeto_02;
    public GameObject Objeto_03;
    public GameObject Objeto_04;
    public GameObject Objeto_05;

    public GameObject Imagen_01;
    public GameObject Imagen_02;
    public GameObject Imagen_03;
    public GameObject Imagen_04;
    public GameObject Imagen_05;

    public GameObject Cuboa_01;
    public GameObject Cubob_01;
    
    public GameObject Cuboa_02;
    public GameObject Cubob_02;

    public GameObject Cuboa_03;
    public GameObject Cubob_03;

    public GameObject Cuboa_04;
    public GameObject Cubob_04;

    public GameObject Cuboa_05;
    public GameObject Cubob_05;

    private Vector3 pivote = new Vector3(0,0,0);
    private Vector3 nueva_imagen = new Vector3(0, 0, 0);
       
    // Use this for initialization
	void Start () 
    {
         
	}
	
	// Update is called once per frame
	void Update () 
    {
        Find_Object(Objeto_01, Cuboa_01, Cubob_01, Imagen_01);
        Find_Object(Objeto_02, Cuboa_02, Cubob_02, Imagen_02);
        Find_Object(Objeto_03, Cuboa_03, Cubob_03, Imagen_03);
        Find_Object(Objeto_04, Cuboa_04, Cubob_04, Imagen_04);
        Find_Object(Objeto_05, Cuboa_05, Cubob_05, Imagen_05);
	}

    void Find_Object (GameObject Objeto, GameObject Pivote1, GameObject Pivote2, GameObject Imagen)
    {
        pivote = GameObject.Find(Objeto.name).transform.position;
        GameObject.Find(Pivote1.name).transform.position = pivote;
        nueva_imagen = GameObject.Find(Pivote2.name).transform.position;
        GameObject.Find(Imagen.name).transform.position = nueva_imagen;    
    }
}
