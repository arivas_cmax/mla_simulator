//#define HAS_SILVERLINING
using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class DemoControls : MonoBehaviour {
	
	private TritonUnity triton 			= null;

    public Slider windSpeedSlidernonVR;
    public Slider windDirectionSlidernonVR;
    public Slider choppinessSlidernonVR;
    public Slider depthSlidernonVR;
    public Toggle sprayTogglenonVR;
    public Canvas nonVRGUI;
    public GameObject nonVRRig;
    public Camera nonVRCamera;

    private Canvas UI;
    private Slider windSpeedSlider;
    private Slider windDirectionSlider;
    private Slider choppinessSlider;
    private Slider depthSlider;
    private Toggle sprayToggle;

	// Use this for initialization
	void Start () {
		triton = (TritonUnity)(GameObject.FindObjectOfType(typeof(TritonUnity)));

        nonVRGUI.enabled = true;
        nonVRRig.SetActive(true);
        triton.gameCamera = nonVRCamera;
        UI = nonVRGUI;
        windSpeedSlider = windSpeedSlidernonVR;
        windDirectionSlider = windDirectionSlidernonVR;
        choppinessSlider = choppinessSlidernonVR;
        depthSlider = depthSlidernonVR;
        sprayToggle = sprayTogglenonVR;

        windSpeedSlider.minValue = 0.0f;
        windSpeedSlider.maxValue = 25.0f;
        windSpeedSlider.value = 5.0f;
        windSpeedSlider.onValueChanged.AddListener(delegate { SpeedChange(); });

        windDirectionSlider.minValue = 0.0f;
        windDirectionSlider.maxValue = 360.0f;
        windDirectionSlider.value = 0.0f;
        windDirectionSlider.onValueChanged.AddListener(delegate { DirectionChange(); });

        choppinessSlider.minValue = 0.0f;
        choppinessSlider.maxValue = 2.0f;
        choppinessSlider.value = 1.6f;
        choppinessSlider.onValueChanged.AddListener(delegate { ChoppinessChange(); });

        depthSlider.minValue = 1.0f;
        depthSlider.maxValue = 1000.0f;
        depthSlider.value = 1000.0f;
        depthSlider.onValueChanged.AddListener(delegate { DepthChange(); });

        sprayToggle.isOn = false;
        sprayToggle.onValueChanged.AddListener(delegate { SprayChange(); });

        
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.Escape))
	    {
	        Application.Quit();
	    }
		
		if (Input.GetKeyDown (KeyCode.C))
		{
            UI.enabled = !UI.enabled;
		}
		
	}

    private void SpeedChange()
    {
        triton.windSpeed = windSpeedSlider.value;
    }

    private void DirectionChange()
    {
        triton.windDirection = windDirectionSlider.value;
    }

    private void ChoppinessChange()
    {
        triton.choppiness = choppinessSlider.value;
    }

    private void DepthChange()
    {
        triton.depth = depthSlider.value;
    }

    private void SprayChange() {
        triton.spray = sprayToggle.isOn;
    }
}