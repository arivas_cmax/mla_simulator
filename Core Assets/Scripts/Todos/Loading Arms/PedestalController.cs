﻿using UnityEngine;
using System.Collections;

public class PedestalController : MonoBehaviour {

	public GameObject tool;
	public GameObject cableStart;
	public GameObject cableEnd;

	public float distanceFactor = 100;

	public float distanceBefore = 0;

	// Use this for initialization
	void Start () {
		//distanceBefore = getDistance();
	}
	
	// Update is called once per frame
	void Update () {
		/*UltimateRope rope = cableStart.GetComponent<UltimateRope>();

		//if(distanceBefore < getDistance())
			//rope.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, getDistance() - distanceBefore);

		rope.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, distanceFactor);

		distanceBefore = getDistance();*/

		if(getDistance() <= distanceFactor)
		{
			tool.SetActive(true);
		}else{
			tool.SetActive(false);
		}
	}

	void OnEnable()
	{
		//cableEnd.SetActive(true);
		//cableStart.GetComponent<UltimateRope>().Regenerate();
		cableStart.SetActive(true);
		//cableStart.GetComponent<UltimateRope>().Regenerate();
		//cableStart.GetComponent<UltimateRope>().AutoRegenerate = true;
	}

	void OnDisable()
	{
		cableStart.SetActive(false);
		//cableEnd.SetActive(false);
	}

	private float getDistance(){
		return (float)Vector3.Distance(cableStart.transform.position, cableEnd.transform.position);
	}
}
