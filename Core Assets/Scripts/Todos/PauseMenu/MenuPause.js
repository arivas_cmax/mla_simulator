﻿// Array of menu item control names.
var mainMenuSceneName : String;
private var pauseEnabled = false;
var pauseMenuFont : Font;
var menuOptions = new String[3];

menuOptions[0] = "Resumen";
menuOptions[1] = "Return";
menuOptions[2] = "Exit";

var Panel : GameObject;

// Default selected menu item (in this case, Tutorial).

private var selectedIndex : int = 0;

// Function to scroll through possible menu items array, looping back to start/end depending on direction of movement.

function Start()
{
    pauseEnabled = false;
    Time.timeScale = 1;
    AudioListener.volume = 1;
}

function Update() 
{
    var pause  = Input.GetButtonDown("Cancel");

	//check if pause button (escape key) is pressed
	if(pause)
	{
	    if (pauseEnabled == true) 
	    {
			Panel.SetActive(false);
			pauseEnabled = false;
			Time.timeScale = 1;
			AudioListener.volume = 1;
	    }

	    //else if game isn't paused, then pause it
	    else if (pauseEnabled == false) 
	    {
	        Panel.SetActive(true);
	        pauseEnabled = true;
	        AudioListener.volume = 0;
	        Time.timeScale = 0;
	    }
    }
    
    if(pauseEnabled)
    {
        if (Input.GetKeyDown(KeyCode.DownArrow)) 
        {
            selectedIndex = menuSelection(menuOptions, selectedIndex, "down");
        }

        if (Input.GetKeyDown(KeyCode.UpArrow)) 
        {
            selectedIndex = menuSelection(menuOptions, selectedIndex, "up");
        }

        if (Input.GetKey("space")) 
        {
            handleSelection();
        }
    }
}

function handleSelection() 
{
    GUI.FocusControl(menuOptions[selectedIndex]);
    switch (selectedIndex) 
    {
        case 0:
        	Panel.SetActive(false);
            pauseEnabled = false;
            Time.timeScale = 1;
            AudioListener.volume = 1;
            Cursor.visible = true;
            break;
        case 1:            
            Application.LoadLevel(mainMenuSceneName);
            break;
        case 2:            
            Application.Quit();
            break;      
        default:            
            break;
    }
}

function OnGUI() 
{
GUI.skin.box.font = pauseMenuFont;
GUI.skin.button.font = pauseMenuFont;
    if (pauseEnabled == true) 
    {     
        GUI.SetNextControlName("Resumen");
        if (GUI.Button(Rect(Screen.width / 2 - 100, Screen.height / 2 - 50, 250, 50), "Resume Simulation")) 
        {
            selectedIndex = 0;
            handleSelection();
        }

        GUI.SetNextControlName("Return");
        if (GUI.Button(Rect(Screen.width / 2 - 100, Screen.height / 2, 250, 50), "Main Menu")) 
        {
            selectedIndex = 1;
            handleSelection();
            
        }
        
        GUI.SetNextControlName("Exit");
        if (GUI.Button(Rect(Screen.width / 2 - 100, Screen.height / 2 + 50, 250, 50), "Exit")) 
        {
            selectedIndex = 2;
            handleSelection();                   
        }
        GUI.FocusControl(menuOptions[selectedIndex]);
    }
}

function menuSelection(menuItems, selectedItem, direction) 
{
	var i;
	i=selectedItem;
	//Debug.Log(selectedItem);


	if (direction == "up")
	{
	    i=-1;
    }else{
	    i=1;
    }

	i = selectedItem + i; 

	if(i==-1)
	{
	    i = 2;
	}else if(i==3){
		i=0;
	}

	selectedItem = i;
	print(selectedItem);
	    
	    return selectedItem;
	}