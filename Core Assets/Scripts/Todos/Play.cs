﻿using UnityEngine;
using System.Collections;

public class Play : MonoBehaviour {

    public int waitTime; // Tiempo de espera en el pase de scene
    public MovieTexture movTexture;
	// Use this for initialization
	void Start () {
        GetComponent<Renderer>().material.mainTexture = movTexture;
        movTexture.Play();

        StartCoroutine(Timer());
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Application.LoadLevel("Menu");
        }        
	}

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(waitTime);
        Application.LoadLevel("Menu");      
    }    
}
