﻿using UnityEngine;
using System.Collections;

public class StateTester : MonoBehaviour {

    private StateManager stateManager;

    public bool makeRight = false;

	// Use this for initialization
	void Start () {
        stateManager = StateManager.getInstance();
        stateManager.startStateMachine();
	}
	
	// Update is called once per frame
	void Update () {
	    if(makeRight)
        {
            stateManager.stateParameters.IsStarted = true;
        }
	}
}
