#pragma strict
#pragma implicit
#pragma downcast

var flySpeed = 15.00;
var highFlyHeight = 80.00;
var normalFlyHeight = 40.00;
var lowFlyHeight = 20.00;
var flyDownSpeed = 0.10;
var circleRadius = 60.00;
var circleSpeed = 0.20;
var circleTime = 15.00;
var awayTime = 20.00;

var offset : Vector3;

private var myT : Transform;;
private var awayDir : Vector3;
private var flyHeight = 0.00;
private var col : Collider;
private var hit : RaycastHit;
private var distToTarget = 0.00;
private var lastHeight = 0.00;
private var height = 0.00;
private var terrainSize : Vector3;
private var terrainData : TerrainData;

private var dTime = 0.1;

function Start ()
{

}

function Move (delta : Vector3)
{
	delta.y = 0;
	delta = delta.normalized * flySpeed * dTime;
	newPos = Vector3(myT.position.x + delta.x, 1000, myT.position.z + delta.z);
	if(col.Raycast(Ray(newPos, -Vector3.up), hit, 2000)) newHeight = hit.point.y;
	else newHeight = 0.00;
	if(newHeight < lastHeight) height = Mathf.Lerp(height, newHeight, flyDownSpeed * dTime);
	else height = newHeight;
	lastHeight = newHeight;
	myT.position = Vector3(newPos.x, Mathf.Clamp(height, 35.28, 1000.00) + flyHeight, newPos.z);
}