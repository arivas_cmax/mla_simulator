﻿using UnityEngine;
using System.Collections;

public class LockIntoPlace : MonoBehaviour {

	public GameObject vessel;
	private Vector3 fixedPosition;

	public ConfigurableJoint hinge1;
	public ConfigurableJoint hinge2;

	public GameObject completeCoupler;

	// Use this for initialization
	void Start () {
        afflojateIt();

		completeCoupler.transform.parent = vessel.transform;
		ERCMovement script = (ERCMovement)completeCoupler.GetComponent("ERCMovement");
		script.enabled = false;
	}

	void OnDisable(){
        endureceIt();
		
		completeCoupler.transform.parent = hinge2.gameObject.transform;
		ERCMovement script = (ERCMovement)completeCoupler.GetComponent("ERCMovement");
		script.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		transform.localPosition = fixedPosition;
	}

    public void afflojateIt()
    {
        transform.parent = vessel.transform;
        fixedPosition = transform.localPosition;

        JointDrive freeWheelDrive = hinge1.angularXDrive;
        freeWheelDrive.positionDamper = 0;
        freeWheelDrive.positionSpring = 0;
        freeWheelDrive.maximumForce = 0;

        hinge1.angularXDrive = freeWheelDrive;
        hinge2.angularXDrive = freeWheelDrive;
    }

    public void endureceIt()
    {
        JointDrive freeWheelDrive = hinge1.angularXDrive;
        freeWheelDrive.positionDamper = 200000;
        freeWheelDrive.positionSpring = 200000;
        freeWheelDrive.maximumForce = 5000;

        hinge1.angularXDrive = freeWheelDrive;
        hinge2.angularXDrive = freeWheelDrive;
    }
}
