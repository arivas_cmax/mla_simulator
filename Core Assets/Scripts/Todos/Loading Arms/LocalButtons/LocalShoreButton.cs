﻿using UnityEngine;
using System.Collections;

public class LocalShoreButton : MonoBehaviour {

	public GameObject controller;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		if(other.tag == "Player")
		{
			Debug.Log("Shore");
			LoadingArmSelector scriptSelector = controller.GetComponent<LoadingArmSelector>();
			LoadingArmController scriptController = scriptSelector.getCurrentArm().GetComponent<LoadingArmController>();
			scriptController.isMovingShore = true;
		}
	}
	
	void OnTriggerExit(Collider other){
		if(other.tag == "Player")
		{
			LoadingArmSelector scriptSelector = controller.GetComponent<LoadingArmSelector>();
			LoadingArmController scriptController = scriptSelector.getCurrentArm().GetComponent<LoadingArmController>();
			scriptController.isMovingShore = false;
		}
	}
}
