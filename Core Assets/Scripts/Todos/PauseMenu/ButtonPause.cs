﻿using UnityEngine;
using System.Collections;

public class ButtonPause : MonoBehaviour 
{ 
	private bool pauseEnabled = false;
	public GameObject Panel;

	void Start()
	{
		pauseEnabled = false;
		Time.timeScale = 1;
		AudioListener.volume = 1;
	}
	
	void Update() 
	{
		var pause  = Input.GetButtonDown("ManualERC");
		
		//check if pause button (escape key) is pressed
		if(pause)
		{
			if (pauseEnabled == true) 
			{
				//unpause the game
				Panel.SetActive(false);
				pauseEnabled = false;
				Time.timeScale = 1;
				AudioListener.volume = 1;
			}
			//else if game isn't paused, then pause it
			else if (pauseEnabled == false) 
			{
				Panel.SetActive(true);
				pauseEnabled = true;
				AudioListener.volume = 0;
				Time.timeScale = 0;
			}
		}
	}
}