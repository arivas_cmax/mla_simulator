﻿using UnityEngine;
using System.Collections;

public class AccidentTrigger : MonoBehaviour {
	TritonUnity triton;
	public GameObject seaCollision;
	private bool justOnceWaterCollision = false;
    public float seaOffset = 0;

    float contadorSplash = 0;

	// Use this for initialization
	void Start () {
		triton = (TritonUnity)(GameObject.FindObjectOfType(typeof(TritonUnity)));
	}
	
	// Update is called once per frame
	void Update () 
	{
		//Chocar con el mar
		if(!justOnceWaterCollision){
			if(gameObject.transform.position.y + seaOffset <= triton.gameObject.transform.position.y)
			{
				/*GameObject seaCollisionClone = Instantiate(seaCollision);
				seaCollisionClone.transform.position = gameObject.transform.position;
				seaCollisionClone.SetActive(true);*/
                triton.timeScale = 1;

                TritonImpact tritonImpact = gameObject.GetComponent<TritonImpact>();
                tritonImpact.trigger = true;

                Debug.Log("Splash!!!1");

				justOnceWaterCollision =  true;
			}
        }
        else
        {
            if (contadorSplash >= 2)
                triton.timeScale = 0.2f;

            contadorSplash += Time.deltaTime;
        }
	}
}
